# Project Sili
## Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

##2018-09-10

###Added
- Code for managing employees

###Changed
- UI for Dashboard

##2018-09-04

###Added
- New components to satisfy the Shift Management flow
	- emp-shift
	- confirmation-component
		- this will serve as a "validation" before the management can permanently disable editing an employee's information

###Changed
- register-component
- emp-management 
- app-routing.module
- app.module

##2018-07-31

###Added
- New components to satisfy the Employee Management flow
	- emp-management
	- register-component
	
###Changed
- app-routing.module.ts
	- to accomodate the new components
	
##2018-07-06
### Added

- Basic compontents into the `app` folder
	- dashboard-component
	- guard
	- login-page
	- mock (temoporary backend/databse)
	- page-not-found
	- services
	- time-table
	- weather-component
	
	
- Angular Material
	- installed to implement modern styling
	- added a `material.module.ts` into the `app` folder to import all possibly used Annguar Material components.

- CHANGELOG.md
    - Added to show updates for each version
	
### Changed

- app.module.ts
	- Updated to accommodate newly added components

- app-routing.module.ts
	- Configured to route particular components.
	
- style.scss
	- Imported a theme called `material-icons.css`
	
- README.md
	- Updated to accomodate future developers.

### Removed

##2018-09-04

- Auto-logout Service & Weather Service
	- has been unused since the 1st demo


