# Sili

Project Sili is a time tracking application that is utilized for managing multiple elements in developing a project. It lets users keep track of their time management and expenses. Project Sili is also allows the management for keeping projects healhy by providing a dashboard with intuitive visual reports. This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## This guide is for those using Ubuntu 16.04 and MAC OS

## Getting started

This section will provide a walkthrough of all the necessary actions to be ready to contribute to the development of the project.

### Prerequisites

These are the necessary things to install before proceeding with development. Please install the following requiremants in order.

* NodeJS
* Angular CLI
* Angular Material - for frontend design and development
* Git

#### NodeJS
install nodejs using node version manager
```
  sudo apt-get install build-essential libssl-dev curl
  curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh -o install_nvm.sh
  bash install_nvm.sh
  source ~/.profile
  nvm ls-remote
  nvm install 8.11.3
  nvm use 8.11.3
  npm i -g npm@6.3.0
  ```

#### Git
```
  sudo apt-get install git-core
  git config --global user.name "sample name"
  git config --global user.email "sample@email.com"
```

### Getting a copy
To get a copy of the project, please clone from the available [repository](https://gitlab.com/amihan/blockchain/sili). It is also possilbe to run this command on the terminal with the desired directory selected to have a copy of the project: 
```
git clone git@gitlab.com:amihan/blockchain/sili.git
cd sili
git checkout sherwin_branch
```

#### Angular CLI
To install the Angular CLI, enter the following command:
```
  npm install -g @angular/cli
```
It is important to have NodeJS installed before installing Angular CLI. To learn more about Angular CLI, please visit either the [Angular CLI page](https://cli.angular.io/) or the [Angular CLI documentation](https://github.com/angular/angular-cli/wiki).

For further instructions on the installation process, visit the [Angular Material installation guide](https://material.angular.io/guide/getting-started). To learn more about Anguar Material, please visit the [Angular Material website](https://material.angular.io/).

#### Angular Material

To install Angular Material, enter the following command in the terminal with the project directory selected (after cloning the project files):
```
  npm rebuild node-sass
  npm install --save @angular/platform-browser-dynamic @angular/material @angular/cdk @angular/animations hammerjs
```

## Development

This section will provide a walkthrough of how development is done.

### Before everything else

After pulling a fresh copy of the project from the repository, run this command in the terminal with the project directoy selected:
```
npm install
```
This command will install its dependencies

### Development server

Run ``npm start`` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Configure  Database
open a new terminal and go to server folder
``cd server``
```
npm install
```

### Adding database (or you can [skip](https://gitlab.com/amihan/blockchain/sili/tree/sherwin_branch#run-database) this part)
go to [mlab](https://mlab.com/) website and sign-up after signing-up click ``create new`` besides create from backup
```
choose sandbox because it's free. click continue
choose region. click continue
enter database name (<--- needed later). continue and submit
```
### Creating database; database username and password
click the newly created database
```
go to users tab
add database user
input username and password (<---- needed later)
```
### Configuring Database
edit ``config.js`` from the server folder and change the values

you can find the ``DsNumber`` and ``Port`` by clicking your database

### Run Database
type in the terminal ``node server.js`` and press ``y or Y`` to create admin account
```
username: admin
password: admin
```

## How to Use
click [docs](https://gitlab.com/amihan/blockchain/sili/blob/sherwin_branch/docs/docs.md)

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Installed Angular Components

Angular Material, Animation, cdk, CookieService, Angular Social Login and Hammerjs
