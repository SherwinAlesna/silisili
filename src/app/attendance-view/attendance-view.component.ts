import { Component } from '@angular/core'

@Component({
  selector: 'app-attendance-view',
  templateUrl: './attendance-view.component.html',
  styleUrls: ['./attendance-view.component.scss']
})
export class AttendanceViewComponent {
  role: string
  show: boolean

  constructor() { 
    this.role = localStorage.getItem('role') 
    if(this.role == 'Manager' || this.role == 'HR' || this.role == 'Supervisor') {
      this.show = true
    }
  }
  save() {
    console.log('saved')
  }
}
