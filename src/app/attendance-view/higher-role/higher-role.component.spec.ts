import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HigherRoleComponent } from './higher-role.component';

describe('HigherRoleComponent', () => {
  let component: HigherRoleComponent;
  let fixture: ComponentFixture<HigherRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HigherRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HigherRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
