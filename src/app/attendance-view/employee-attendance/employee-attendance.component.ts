import { Component, OnInit } from '@angular/core'
import { Apollo } from 'apollo-angular'
import { attendance, Query, employee } from '../../mock/model'
import { MatDialog, MatDialogConfig } from '@angular/material'
import { GetAllEmp, GetAllAttendances, ApproveAllTime, GetEmployee,
  NewClockin, NewClockout, UpdateTime, UpdateAllTime, RejectTime } from '../../apollo'
import { ChangeTime } from '../../dialog/dialog-clockin'
import { TimeStatus } from '../../dialog/dialog-time-status'
import { Notify } from '../../dialog/dialog-notify'

@Component({
  selector: 'app-employee-attendance',
  templateUrl: './employee-attendance.component.html',
  styleUrls: ['./employee-attendance.component.scss']
})
export class EmployeeAttendanceComponent implements OnInit {

  empId: string
  defaultTime: string = '00:00 AM'
  empData: any = []
  role: string
  show: boolean = false
  currTimeIn: string
  currTimeOut: string
  rowNewTimeIn: string
  rowNewTimeOut: string
  rowAttId: string
  result: any
  periods: any = [
    {value: 'AM'},
    {value: 'PM'}
  ]
  isDisabled: boolean = true
  newTimeIn: any
  newTimeOut: any

  constructor(private apollo: Apollo,private dialog: MatDialog) { }

  ngOnInit() {
    this.role = localStorage.getItem('role')
    if(this.role !== 'Employee') {
      this.show=true
      this.isDisabled=false
    }
    this.empId = localStorage.getItem('ID')
    this.getAllAtt()
  }
  getAllAtt() {
    let supervisors
    let empName
    this.apollo.watchQuery<Query>({
      query: GetEmployee,
      variables: {
        id: this.empId
      }
    }).valueChanges.subscribe(emp => {
      supervisors = emp.data.employee.work_details.supervisor
      empName = emp.data.employee.person_details.first_name
      this.apollo.watchQuery<Query>({
        query: GetAllAttendances
      }).valueChanges.subscribe(att => {
        let emps = att.data.allAttendance
        for(let i of emps) {
          // change to identifaction number later
          if(i.supervisor == empName) {
            if(`${i.role}` == 'Employee') {
              this.empData.push(i)
            }
          }
          else if(i.supervisor == supervisors) {
            if(`${i.role}` == 'Employee') {
              this.empData.push(i)
            }
          }
        }
      })
    })
  }
  rowClicked(i) {
    this.currTimeIn = i.clockin
    this.currTimeOut = i.clockout
    this.rowNewTimeIn = i.newClockin
    this.rowNewTimeOut = i.newClockout
    this.rowAttId = i.attId
    console.log(i)
  }
  clockTime(type) {
    let dConfig = new MatDialogConfig()
    dConfig.data = {
      title: type,
      message: `Change ${type} Time`,
      type
    }
    this.onConfirmDialog(dConfig)
  }
  onConfirmDialog(data) {
    this.dialog.open(ChangeTime, data).afterClosed().subscribe(res => {
      this.result = res[0]
      let type = this.result.type
      let time = this.result.time
      let period = this.result.period
      if(type == 'Clockin') {
        this.flagClockin(time,period)
      } else {
        this.flagClockout(time,period)
      }
    })
  }
  flagClockin(time,period)  {
    this.apollo.mutate({
      mutation: NewClockin,
      variables: {
        attId: this.rowAttId,
        in: this.currTimeIn,
        newClockin: `${time} ${period}`,
        app: 'For_Approved'
      }
    }).subscribe(() => {
      this.notify()
    })
  }
  flagClockout(time,period) {
    this.apollo.mutate({
      mutation: NewClockout,
      variables: {
        attId: this.rowAttId,
        out: this.currTimeOut,
        newClockout: `${time} ${period}`,
        app: 'For_Approved'
      }
    }).subscribe(() => {
      this.notify()
    })
  }
  notify() {
    this.dialog.open(Notify)
  }
  approveAttendance() {
    let dConfig = new MatDialogConfig()
    dConfig.data = {
      title: 'Cofirm New Time',
      message: 'approve new time?',
      type: 'newTime',
      clockin: this.currTimeIn,
      clockout: this.currTimeOut,
      newClockin: this.rowNewTimeIn,
      newClockout: this.rowNewTimeOut
    }
    this.dialog.open(TimeStatus,dConfig).afterClosed().subscribe(res => {
      if(res == 'rejected') {
        this.rejectTime()
      } else if(res == 'newTime') {
        this.updateNewTime()
      }
    })
  }
  rejectTime() {
    this.apollo.mutate({
      mutation: RejectTime,
      variables: {
        attId: this.rowAttId,
        newClockin: this.defaultTime,
        newClockout: this.defaultTime,
        app: 'Not_Approved'
      }
    }).subscribe(() => {
      console.log('attendance rejeced')
    }, err => {
      console.log(`Error: ${err}`)
    })
  }
  // WIP 1
  checkTime(time,type) {
    if(time == this.defaultTime) {
      if(type == 'timeOut') {
        this.newTimeOut = this.currTimeOut
      }
      else if (type == 'timeIn') {
        this.newTimeIn = this.currTimeIn
      }
    }

    return [this.newTimeIn,this.newTimeOut]
  }
  // ##
  updateNewTime() {
    // WIP 1:
    this.checkTime(this.rowNewTimeIn,'timeIn')
    this.checkTime(this.rowNewTimeOut,'timeOut')
    console.log(`${this.newTimeIn} newtimein,${this.newTimeOut} newtimeout`)
    // ##

    if(this.rowNewTimeIn != this.defaultTime || this.rowNewTimeOut != this.defaultTime) {
      this.apollo.mutate({
        mutation: UpdateTime,
        variables: {
          attId: this.rowAttId,
          in: this.rowNewTimeIn,
          out: this.rowNewTimeOut,
          newClockin: this.rowNewTimeIn,
          newClockout: this.rowNewTimeOut,
          app: 'Approved'
        }
      }).subscribe(() => {
        console.log('attendance approved')
      }, err => {
        console.log(`Error: ${err}`)
      })
    }
  }
  updateApproveTime(clockin,clockinPeriod,clockout,clockoutPeriod,type) {
    type == 'updateAllTime' ? 
      this.onUpdateAllTime(clockin,clockinPeriod,clockout,clockoutPeriod)
      : this.onApproveAllTime()
  }
  onUpdateAllTime(clockin,clockinPeriod,clockout,clockoutPeriod) {
    this.apollo.mutate({
      mutation: UpdateAllTime,
      variables: {
        id: this.empId,
        newClockin: `${clockin} ${clockinPeriod}`,
        newClockout: `${clockout} ${clockoutPeriod}`,
        app: 'For_Approved'
      }
    }).subscribe(() => {}, () => {})
  }
  onApproveAllTime() {
    for(let i of this.empData) {
      let newCI = i.newClockin
      let newCO = i.newClockout
      let attId = i.attId

      if(i.approved == 'For_Approved') {
        this.apollo.mutate({
          mutation: ApproveAllTime,
          variables: {
            attId,
            in: newCI,
            out: newCO,
            newClockin: this.defaultTime,
            newClockout: this.defaultTime,
            app: 'Approved'
          }
        }).subscribe(() => {}, () => {})
      }
    }
  }
}
