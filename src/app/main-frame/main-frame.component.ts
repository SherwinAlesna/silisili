import { Component, OnInit, Output, OnChanges } from '@angular/core'
import { Router } from '@angular/router'
import { AuthService , SocialUser} from "angularx-social-login"
import { CookieService } from 'ngx-cookie-service'
import { NavbarService } from '../services/navbar.service'
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material'
import { TimeStatus } from '../dialog/dialog-time-status'
import { Apollo } from 'apollo-angular'
import { Query } from '../mock/model'
import { Clockin, Clockout, GetAllAttendances, GetEmployee } from '../apollo'
let dateFormat = require('dateformat')

@Component({
  selector: 'app-main-frame',
  templateUrl: './main-frame.component.html',
  styleUrls: ['./main-frame.component.scss']
})
export class MainFrameComponent implements OnInit {
  id: string
  late: boolean
  time: Date
  color: string = "warn"
  tooltip: string = "Click to time in"
  show1: string = "show"
  show2: string = "hide"
  btnText: string = "You have not yet clocked in!"
  manager_view: string = "hide"
  name : string
  email : string
  pimg : string
  userProfile : any
  interval: any
  defaultApproved: string = 'Not_Approved'
  defaultTime: string = '00:00 AM'
  underTime: boolean
  lastId: any
  shiftEnd: any
  clockinTime: any
  user: SocialUser

  constructor(
    private router: Router,
    private authService: AuthService,
    private cookieService: CookieService,
    private dialog: MatDialog,
    private apollo: Apollo
  ) { }
  
  ngOnInit() {
    let dConfig = new MatDialogConfig()
    dConfig.data = {
      title: 'Clockin',
      message: 'Do you want to Clock in?',
      type: 'clockin'
    }
    if(localStorage.getItem('TI') == 'null') {
      setTimeout(() => this.dialog.open(TimeStatus,dConfig).afterClosed().subscribe(res => {
        this.onConfirmDialog(res)
      }))
    }
    else if(localStorage.getItem('TO')){
      this.btnText = "Already clocked in for today"
    }

    if(this.cookieService.get('profile')) {
      //some components can only be seen by the manager
      //shows manager view if true
      if(localStorage.getItem('role') == 'Manager' || localStorage.getItem('role') == 'Supervisor'
      || localStorage.getItem('role') == 'HR') {
        this.manager_view = "show"
      }
      this.userProfile = JSON.parse(this.cookieService.get('profile'))
      this.pimg = this.userProfile['photoUrl'] // how??
      this.name = localStorage.getItem('name')
      this.email = localStorage.getItem('email')
    }
    else {
      this.router.navigate(['/login'])
    }
    this.id = localStorage.getItem('ID')
    this.getAttendanceLastId()
    // this.getPicture() // fail input
  }
  getPicture() {
    this.authService.authState.subscribe(user => {
      this.user = user
    })
  }
  getAttendanceLastId() {
    this.apollo.watchQuery<Query>({
      query: GetAllAttendances
    }).valueChanges.subscribe(res => {
      for(let i of res.data.allAttendance) {
        this.lastId = i.attId
      }
      return this.lastId
    })
  }
  empAttendance() {
    // change condition to??
    if(this.color == 'accent') {
      this.onConfirmDialog('clockout')
    }
    else {
      this.onConfirmDialog('clockin')
    }

    // if(localStorage.getItem('TO') != 'null') {
    //   return null
    // } 
    // else {
    //   if(localStorage.getItem('TI') == 'null') {
    //     this.onConfirmDialog('clockin')
    //   } 
    //   else {
    //     this.onConfirmDialog('clockout')
    //   }
    // }
  }
  onConfirmDialog(res) {
    if(res == 'clockin') {
      this.clockIn()
      this.btnText = 'clocked in since: ' + this.time.toString()
      this.color = "accent"
      this.tooltip = "Click to clock out"
    }
    else if(res == 'clockout') {
      this.clockOut()
      this.btnText = "Already clocked in for today"
      this.color = "warn"
      this.tooltip = "Clocked in for today"
    }
  }
  // bug/glich?? change time manually according to your shift start:
  checkLate(d) {
    let shiftStart = d.data.employee.shifts.shiftTimeStart.substr(0,2)
    let shiftStartPer = d.data.employee.shifts.shiftTimeStart.substr(6,2)
    let timeNow = this.time.toString().substr(0,2)
    let timeNowPer = this.time.toString().substr(6,2)
    if(shiftStartPer == timeNowPer) {
      if(parseInt(shiftStart) < parseInt(timeNow)) {
        console.log(`if: ${timeNow} ${timeNowPer} / ${shiftStart} ${shiftStartPer}`)
        this.late = true
      }
      else if(parseInt(shiftStart) == 12 && parseInt(shiftStart) > parseInt(timeNow)) {
        console.log(`if:else if 1: ${shiftStart} ${shiftStartPer} / ${timeNow} ${timeNowPer}`)
        this.late = true
      }
      else if(parseInt(timeNow) < parseInt(shiftStart)) {
        console.log(`if:else if 2: ${shiftStart} ${shiftStartPer} / ${timeNow} ${timeNowPer}`)
        this.late = false
      }
      else {
        console.log(`if:else ${shiftStart} ${shiftStartPer} / ${timeNow} ${timeNowPer}`)
        this.late = false
      }
    }
    else {
      console.log(`else: ${shiftStart} ${shiftStartPer} / ${timeNow} ${timeNowPer}`)
      if(parseInt(timeNow) == parseInt(shiftStart)-1 && shiftStart == '12' && shiftStartPer == 'AM') {
        console.log(`else:if ${shiftStart} ${shiftStartPer} / ${timeNow} ${timeNowPer}`)
        this.late = false
      }
      else if(parseInt(timeNow) == parseInt(shiftStart)-1 && shiftStart == '12' && shiftStartPer == 'PM') {
        console.log(`else: else if 1: ${shiftStart} ${shiftStartPer} / ${timeNow} ${timeNowPer}`)
        this.late = false
      }
      else {
        console.log(`else:else ${shiftStart} ${shiftStartPer} / ${timeNow} ${timeNowPer}`)
        this.late = true
      }
    }
    console.log('late: ',this.late)
    return this.late
  }
  clockIn() {
    let now = new Date()
    let date = dateFormat(now, 'mm-dd-yyyy')
    let attId
    this.lastId == null ? attId = 1 : attId = parseInt(this.lastId) + 1
    this.time = dateFormat(now, 'hh:MM TT')
    this.clockinTime = this.time
    localStorage.setItem('TI', this.time.toString())
    this.apollo.watchQuery<Query>({
      query: GetEmployee,
      variables: {
        id: localStorage.getItem('ID')
      }
    }).valueChanges.subscribe(d => {
      this.checkLate(d)
      this.shiftEnd = d.data.employee.shifts.shiftTimeEnd
      let emp = d.data.employee
      attId = String(attId)
      this.apollo.mutate({
        mutation: Clockin,
        variables: {
          id: emp.id,
          attId,
          emp: {
            first_name: emp.person_details.first_name,
            middle_name: emp.person_details.middle_name,
            last_name: emp.person_details.last_name,
            birthday: emp.person_details.birthday,
            mobile_number: emp.person_details.mobile_number,
            email: emp.person_details.email,
            home_address: emp.person_details.home_address
          },
          date,
          in: this.time.toString(),
          out: this.defaultTime,
          app: this.defaultApproved,
          newClockin: this.defaultTime,
          newClockout: this.defaultTime,
          role: emp.work_details.designation,
          supervisor: emp.work_details.supervisor,
          late: this.late
        }
      }).subscribe(() => {}, err => {console.log('Error', err)})
    })
    let dConfig = new MatDialogConfig()
    dConfig.data = {
      title: 'Clockout',
      message: 'Do you want to Clock out?',
      type: 'clockout'
    }
    this.interval = setInterval(() => {
      this.dialog.open(TimeStatus, dConfig).afterClosed().subscribe(res => {
        this.onConfirmDialog(res)
      })
    }, (1000 * 5)) /*(1000 * 60 * 60 * 9))*/ // = 9hrs
  }
  clockOut() {
    let now = new Date()
    this.time = dateFormat(now, 'hh:MM TT')
    localStorage.setItem('TO', this.time.toString())
    this.checkUnderTime(this.id, this.time)
    console.log('undertime:', this.underTime)
    this.apollo.mutate({
      mutation: Clockout,
      variables: {
        id: this.id,
        out: this.time.toString(),
        app: 'Not_Approved',
        underTime: this.underTime
      }
    }).subscribe(() => {}, err => {})
    clearInterval(this.interval)
  }
  // bug/glich?? change time manually; according to your shift end:
  checkUnderTime(id, time) {
    let timeNowHour = time.substr(0,2) // use this later: change editedTimeNowHour to this
    let editedTimeNowHour = parseInt(timeNowHour) + 9 // for testing
    let timeNowMinutes = time.substr(3,2)
    let ifTimeEnd = [1,2,3,4,5,6,7,8,9]

    // what if early clockin??
    let clockinMinutes = this.clockinTime.substr(3,2)
    let shiftEndHour = parseInt(this.clockinTime.substr(0,2)) + 9
    let addZeroTimeHour
    let editedAddZeroTimeHour
    if(shiftEndHour > 12) {
      shiftEndHour -= 12
      editedTimeNowHour -= 12
      console.log(`
        if:
        early shift end ${shiftEndHour}:${clockinMinutes}
      `)
      if(timeNowHour == shiftEndHour && timeNowMinutes == clockinMinutes) {
        console.log(`
          if:if:
          minutes ${timeNowMinutes} / ${clockinMinutes}
          hour ${timeNowHour} / ${shiftEndHour}
         `)
        this.underTime = false
      }
      else {
        console.log(timeNowHour)
        for(let v of ifTimeEnd) {
          // if shift end hour is 1 ~ 9: add 0 at first letters
          if(v == shiftEndHour) {
            addZeroTimeHour = '0' + v.toString()
            timeNowHour = '0' + v.toString()
            editedAddZeroTimeHour = '0' + v.toString() // for testing
            // shiftEndHour = parseInt(convert)
            // console.log(shiftEndHour)
          }
        }
        console.log(`
          if:else:
          minutes ${timeNowMinutes} / ${clockinMinutes}
          hour ${timeNowHour} / ${shiftEndHour}
        `)
        // over time??
        this.underTime = true
      }
    }
    else {
      console.log(`${timeNowHour} / ${shiftEndHour}`)
      if(timeNowHour == shiftEndHour && timeNowMinutes == clockinMinutes) {
        console.log(`
          else:if:
          minutes ${timeNowMinutes} / ${clockinMinutes}
          hour ${timeNowHour} / ${shiftEndHour}
         `)
        this.underTime = false
      }
      else {
        // over time??
        console.log(`
          else:else:
          minutes ${timeNowMinutes} / ${clockinMinutes}
          hour ${timeNowHour} / ${shiftEndHour}
         `)
        this.underTime = true
      }
    }
    return this.underTime
  }
  signOut() {
    if(this.cookieService.get('photoUrl')) {
      this.authService.signOut()
    }
    localStorage.clear()
    sessionStorage.clear()
    this.cookieService.deleteAll()
    this.router.navigate(['/login'])
  }
}
