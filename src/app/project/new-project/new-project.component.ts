import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms'


export interface Client { 
  value: string
  viewValue: string
}


@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss']
})


export class NewProjectComponent implements OnInit {

  isLinear = true
  clientFormGroup: FormGroup
  projectFormGroup: FormGroup

  clients: Client[] = [
    {value: 'SunLife', viewValue: 'Sun Life'},
    {value: 'BDO', viewValue: 'BDO'},
    {value: 'Others', viewValue: 'Others'}
  ]


  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.clientFormGroup = new FormGroup({
      clientName: new FormControl(),
      nClient: new FormControl(),
      clientAdd: new FormControl(),
      conFName: new FormControl(),
      conMidInit: new FormControl(),
      conLName: new FormControl(),
      conNumber: new FormControl(),
      conEmail: new FormControl()
    })
    this.projectFormGroup = new FormGroup({
      companyName: new FormControl()
    })
  }
}
