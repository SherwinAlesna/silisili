import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { GoogleLoginProvider, AuthServiceConfig, SocialLoginModule } from 'angularx-social-login'
import { CookieService } from 'ngx-cookie-service'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { LoginPageComponent } from './login-page/login-page.component'
import { DashboardComponentComponent } from './dashboard/dashboard-component.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { MaterialModule } from './material.module'
import { AuthGuard } from './guard'
import { WeatherComponentComponent } from './weather-component/weather-component.component'
import { HttpClientModule } from '@angular/common/http' 
import { TimeTableComponent } from './time-table/time-table.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AutoLogoutService } from './services/logout/ index'
import { LayoutModule } from '@angular/cdk/layout'
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material'
import { CountdownModule } from 'ngx-countdown'
import { FooterComponent } from './footer/footer.component'
import { WeekTableComponent } from './time-table/week-table/week-table.component'
import { DayTableComponent } from './time-table/day-table/day-table.component'
import { ProjectComponentComponent } from './project/project-component.component'
import { ActiveProjectComponent } from './project/active-project/active-project.component'
import { NewProjectComponent } from './project/new-project/new-project.component'
import { NotRegisteredComponent } from './not-registered/not-registered.component'
import { ApolloModule, Apollo  } from 'apollo-angular'
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { EmpManagementComponent } from './manage/emp-management/emp-management.component'
import { RegisterComponentComponent } from './manage/register-component/register-component.component'
import { NavbarService } from './services/navbar.service'
import { LogData} from './services/login.service'
import { NavigateAwayService } from './services/navigate.away.service'
import { ConfirmationComponentComponent } from './manage/confirmation-component/confirmation-component.component'
import { EmpShiftComponent } from './manage/emp-shift/emp-shift.component'
import { CreateShiftComponent } from './manage/create-shift/create-shift.component'
import { ChangeShift, AddShift } from './dialog/dialog'
import { ChangeTime } from './dialog/dialog-clockin'
import { Notify } from './dialog/dialog-notify'
import { TimeStatus } from './dialog/dialog-time-status'
import { ManageEmployee } from './dialog/dialog-manageEmp'
import { AttendanceViewComponent } from './attendance-view/attendance-view.component'
import { MainFrameComponent } from './main-frame/main-frame.component';
import { EmployeeAttendanceComponent } from './attendance-view/employee-attendance/employee-attendance.component';
import { HigherRoleComponent } from './attendance-view/higher-role/higher-role.component'

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("1021336407843-vskuovqcksc3pjqi6368ua6f35190cqf.apps.googleusercontent.com")
  }
])

export function provideConfig() {
  return config
}


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    DashboardComponentComponent,
    PageNotFoundComponent,
    WeatherComponentComponent,
    TimeTableComponent,
    FooterComponent,
    WeekTableComponent,
    DayTableComponent,
    ProjectComponentComponent,
    ActiveProjectComponent,
    NewProjectComponent,
    NotRegisteredComponent,
    EmpManagementComponent,
    RegisterComponentComponent,
    ConfirmationComponentComponent,
    EmpShiftComponent,
    CreateShiftComponent,
    ChangeShift,
    AddShift,
    TimeStatus,
    Notify,
    ManageEmployee,
    ChangeTime,
    AttendanceViewComponent,
    MainFrameComponent,
    EmployeeAttendanceComponent,
    HigherRoleComponent
  ],
  entryComponents: [
    ConfirmationComponentComponent,
    ChangeShift,
    AddShift,
    TimeStatus,
    Notify,
    ManageEmployee,
    ChangeTime
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    SocialLoginModule,
    HttpClientModule,
    ApolloModule,
    HttpLinkModule,
    FormsModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    CountdownModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    AuthGuard,
    CookieService,  
    NavbarService,
    LogData,
    NavigateAwayService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(apollo: Apollo,httpLink: HttpLink) {
    apollo.create({
      link: httpLink.create({ uri: 'http://localhost:4000/graphql' }),
      cache: new InMemoryCache()
    })
  } 
}
