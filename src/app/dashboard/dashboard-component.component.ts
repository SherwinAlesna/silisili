import { Component, OnInit } from '@angular/core'
import { NavbarService } from '../services/navbar.service'
import { Apollo } from 'apollo-angular'
import { Query } from '../mock/model'
import { MatDialog, MatDialogConfig } from '@angular/material'
import { TimeStatus } from '../dialog/dialog-time-status'
import { GetAllEmp, DefaultShift } from '../apollo'
var dateFormat = require('dateformat')

@Component({
  selector: 'app-dashboard-component',
  templateUrl: './dashboard-component.component.html',
  styleUrls: ['./dashboard-component.component.scss']
})
export class DashboardComponentComponent implements OnInit {
  private empData: any
  private dateTo: any
  private name: String
  private email: String
  private manager_view: String = "hide"
  private manager: Boolean

  constructor(
    public nav: NavbarService,
    private apollo: Apollo,
    private dialog: MatDialog
  ) { }

  ngOnInit() { 
    this.name =  localStorage.getItem('name').toString()
    this.email = localStorage.getItem('email').toString()
    if(localStorage.getItem('role') == 'Manager' || localStorage.getItem('role') == 'HR'
    || localStorage.getItem('role') == 'Supervisor') {
      this.manager_view = "show"
      this.manager = true
      this.manager_view = "visible"
      this.apollo.watchQuery<Query>({
        query: GetAllEmp
      }).valueChanges.subscribe(d => {
        this.empData = d.data.employees
        this.regularShift(this.empData)
      })
    }
  }
  regularShift(data) {
    for(let i in data) {
      this.dateTo = data[i].shifts.shiftDateTo
      let now = new Date()
      let shiftEnded = dateFormat(now, 'dddd, mm-dd-yyyy')
      let newShiftEnd = dateFormat(new Date(now.getFullYear(),now.getMonth(),now.getDate() - now.getDay() + 9), 'dddd, mm-dd-yyyy')
      let name = data[i].person_details.first_name
      let dConfig = new MatDialogConfig()
      dConfig.data = {
        title: 'Shift Ended',
        message: name + ': Shift Changed to Regular',
        type: 'shift'
      }
      if(this.dateTo == shiftEnded) {
        this.dialog.open(TimeStatus,dConfig)
        this.apollo.mutate({
          mutation: DefaultShift,
          variables: {
            id: this.empData[i].id,
            emp_info: {
              identification_number: this.empData[i].employee_information.identification_number,
              position: this.empData[i].employee_information.position,
              shift: 'Regular',
              status: this.empData[i].employee_information.status
            },
            shifts: {
              shiftDateFrom: shiftEnded,
              shiftDateTo: newShiftEnd,
              shiftTimeStart: '10:00 AM',
              shiftTimeEnd: '07:00 PM'
            }
          }
        }).subscribe(() => {}, err => {})
      }
    }
  }
}