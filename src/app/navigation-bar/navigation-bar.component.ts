// import { Component, OnInit, Output, OnChanges } from '@angular/core'
// import { Router } from '@angular/router'
// import { AuthService } from "angularx-social-login"
// import { CookieService } from 'ngx-cookie-service'
// import { NavbarService } from '../services/navbar.service'
// import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material'
// import { TimeStatus } from '../dialog/dialog-time-status'
// import { LogData } from '../services/login.service'
// import { Apollo } from 'apollo-angular'
// import { Query } from '../mock/model'
// import { Clockin, Clockout, GetAllAttendances } from '../apollo'
// let dateFormat = require('dateformat')

// //All localStorage are only temporary!      ------------------------------------------------
// //Database will be integrated soon          ------------------------------------------------
// //which will replace the use for            ------------------------------------------------
// //localStorage                              ------------------------------------------------

// @Component({
//   selector: 'navigation-bar',
//   templateUrl: './navigation-bar.component.html',
//   styleUrls: ['./navigation-bar.component.scss']
// })
// export class NavigationBarComponent implements OnInit {

//   id: string
//   time: Date
//   color: String = "warn"
//   tooltip: String = "Click to time in"
//   show1: String = "show"
//   show2: String = "hide"
//   btnText: String = "You have not yet clocked in!"
//   manager_view: String = "hide"
//   name : String
//   email : String
//   pimg : String
//   userProfile : any
//   interval: any
//   defaultApproved: string = 'Not_Approved'
//   defaultTime: string = '00:00 AM'
//   notification: any = []

//   constructor(
//     private router: Router,
//     private authService: AuthService,
//     private cookieService: CookieService,
//     private dialog: MatDialog,
//     private datas: LogData,
//     private apollo: Apollo
//   ) { }
  
//   ngOnInit() {
//     let dConfig = new MatDialogConfig()
//     dConfig.data = {
//       title: 'Clockin',
//       message: 'Do you want to Clock in?',
//       type: 'clockin'
//     }
//     if(localStorage.getItem('TI') == 'null') {
//       setTimeout(() => this.dialog.open(TimeStatus,dConfig).afterClosed().subscribe(res => {
//         this.onConfirmDialog(res)
//       }))
//     }
//     if(this.cookieService.get('profile')) {
//       console.log(this.cookieService.get('profile'))
//       //some components can only be seen by the manager
//       //shows manager view if true
//       if(localStorage.getItem('role') == 'Manager') {
//         this.manager_view = "show"
//       }
//       this.userProfile = JSON.parse(this.cookieService.get('profile'))
//       this.name = this.userProfile['name']
//       this.email = this.userProfile['email']
//       this.pimg = this.userProfile['photoUrl']
//     }
//     else {
//       this.router.navigate(['/login'])
//     }

//     if(localStorage.getItem('TI') != 'null' && localStorage.getItem('TO') == 'null') {
//       this.manualTimeIn()
//     }
//     if(localStorage.getItem('TI') != 'null' && localStorage.getItem('TO') != 'null') {
//       this.manualTimeOut()
//     }
//     this.id = localStorage.getItem('ID')
//     // this.getFlag()
//   }
//   manualTimeIn() {
//     this.btnText = 'clocked in since: ' + localStorage.getItem('TI')
//     this.color = "accent"
//     this.tooltip = "Click to clock out"
//   }
//   manualTimeOut() {
//     this.btnText = 'Already Clocked in for today'
//     this.color = "warn"
//     this.tooltip = "Clocked in for today"
//     clearInterval(this.interval)
//   }
  
//   signOut() {
//     this.authService.signOut()
//     localStorage.clear()
//     sessionStorage.clear()
//     this.cookieService.deleteAll()
//     this.router.navigate(['/login'])
//     // console.log('call data',this.notification.approved)
//   }

//   empAttendance() {
//     if(localStorage.getItem('TO') != 'null') {
//     } 
//     else {
//       if(localStorage.getItem('TI') == 'null') {
//         this.onConfirmDialog('clockin')
//       } 
//       else {
//         this.onConfirmDialog('clockout')
//       }
//     }
//   }
//   onConfirmDialog(res) {
//     if(res == 'clockin') {
//       this.clockIn()
//       this.btnText = 'clocked in since: ' + this.time.toString()
//       this.color = "accent"
//       this.tooltip = "Click to clock out"
//     }
//     else if(res == 'clockout') {
//       this.clockOut()
//       this.btnText = "Already clocked in for today"
//       this.color = "warn"
//       this.tooltip = "Clocked in for today"
//     }
//   }

//   clockIn() {
//     let user = this.datas.userDataSource.value[0].person_details
//     let now = new Date()
//     let date = dateFormat(now, 'mm-dd-yyyy')
//     this.time = dateFormat(now, 'hh:MM TT')
//     localStorage.setItem('TI', this.time.toString())

//     this.apollo.mutate({
//       mutation: Clockin,
//       variables: {
//         id: this.id,
//         emp: {
//           first_name: user.first_name,
//           middle_name: user.middle_name,
//           last_name: user.last_name,
//           birthday: user.birthday,
//           mobile_number: user.mobile_number,
//           email: user.email,
//           home_address: user.home_address
//         },
//         date,
//         in: this.time.toString(),
//         out: this.defaultTime,
//         app: this.defaultApproved,
//         newClockin: this.defaultTime,
//         newClockout: this.defaultTime
//       }
//     }).subscribe(() => {}, err => {
//       console.log('Error', err)
//     })

//     let dConfig = new MatDialogConfig()
//     dConfig.data = {
//       title: 'Clockout',
//       message: 'Do you want to Clock out?',
//       type: 'clockout'
//     }
//     this.interval = setInterval(() => {
//       this.dialog.open(TimeStatus, dConfig).afterClosed().subscribe(res => {
//         this.onConfirmDialog(res)
//       })
//     }, (1000 * 70)) // 1000 * 60 * 60 * 9 = 9hrs
//   }
//   clockOut() {
//     let now = new Date()
//     this.time = dateFormat(now, 'hh:MM TT')
//     localStorage.setItem('TO', this.time.toString())
    
//     this.apollo.mutate({
//       mutation: Clockout,
//       variables: {
//         id: this.id,
//         out: this.time.toString(),
//         app: 'Not_Approved'
//       }
//     }).subscribe(() => {}, err => {})
//     clearInterval(this.interval)
//   }
// }
