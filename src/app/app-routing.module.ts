import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuard } from './guard';
import { DashboardComponentComponent } from './dashboard/dashboard-component.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WeatherComponentComponent } from './weather-component/weather-component.component';
import { TimeTableComponent } from './time-table/time-table.component';
import { ProjectComponentComponent } from './project/project-component.component';
import { EmpManagementComponent } from './manage/emp-management/emp-management.component'
import { RegisterComponentComponent } from './manage/register-component/register-component.component'
import { AppComponent } from './app.component';
import { NotRegisteredComponent } from './not-registered/not-registered.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { NavigateAwayService } from './services/navigate.away.service';
import { WeekTableComponent } from './time-table/week-table/week-table.component';
import { DayTableComponent } from './time-table/day-table/day-table.component'
import { ConfirmationComponentComponent } from './manage/confirmation-component/confirmation-component.component';
import { EmpShiftComponent } from './manage/emp-shift/emp-shift.component'
import { AttendanceViewComponent } from './attendance-view/attendance-view.component';
import { MainFrameComponent } from './main-frame/main-frame.component'

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        component: LoginPageComponent,
        canDeactivate: [NavigateAwayService],
    },
    {
        path: 'home',
        component: MainFrameComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full'
            },
            {
                path: 'dashboard',
                component: DashboardComponentComponent,
            },
            {
                path: 'time-sheet',
                component: TimeTableComponent,
                children: [
                    { 
                       path: '#weekly',
                       component: WeekTableComponent
                    },
                    {
                        path: '#daily',
                        component: DayTableComponent
                    }
                ]
            }, 
            {
                path: 'projects',
                component: ProjectComponentComponent
            },
            {
                path: 'manage',
                component: EmpManagementComponent,
                children: [
                    {
                        path:'registration',
                        component: RegisterComponentComponent
                    },
                    {
                        path:'deactivate',
                        component: ConfirmationComponentComponent
                    },
                    {
                        path:'create-shift',
                        component: EmpShiftComponent
                    }
                ]
            },
            {
                path: 'attendance',
                component: AttendanceViewComponent
            },
        ]
    },
    {
        path: '401',
        component: NotRegisteredComponent
    },
    {
        path: '**',
        component: PageNotFoundComponent
    },
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule{
    
}
