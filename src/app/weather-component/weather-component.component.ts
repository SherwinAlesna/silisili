import { Component, OnDestroy, OnInit } from '@angular/core';
import { get } from 'https';
import { HttpClient, HttpClientJsonpModule } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { WeatherService } from '../services/weather/weather.service';


@Component({
  selector: 'app-weather-component',
  templateUrl: './weather-component.component.html',
  styleUrls: ['./weather-component.component.scss']
})
export class WeatherComponentComponent implements OnInit{


  lat: number
  lng: number
  forecast: Observable<any>

  constructor(private weather: WeatherService) {}
  ngOnInit() { 
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude
        this.lng = position.coords.longitude
      })
    } else {
      console.log("Cannot get location")
    }

    console.log(this.lat, this.lng)
  }

  

}
