import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  lat: number
  lng: number 
  key: string = "ab153efed3bcc0d7196e3905e32505bb"
  url: string = "https://api.darksky.net/forecast/" + this.key + "/"
  ngOnInit() { 
  /*   if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat =(position.coords.latitude)
        this.lng =(position.coords.longitude)
      })
    } */

    
  }

  constructor(private http: HttpClient) { }

  currentForecast(lat: number, lng: number): Observable<any> {
    let params = new HttpParams()
    params = params.set('lat', lat.toString() )
    params = params.set('lng', lng.toString() )

    return this.http.get(this.url, { params })
  }
    


}
