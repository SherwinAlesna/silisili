import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'

@Injectable()
export class LogData {
  constructor() {}

  userDataSource: BehaviorSubject<Array<any>> = new BehaviorSubject([])
  userData = this.userDataSource.asObservable()

  addData(data) {
    // this.userDataSource.next(data)
    const currValue = this.userDataSource.value
    const updatedValue = [...currValue, data]
    this.userDataSource.next(updatedValue)
  }
}