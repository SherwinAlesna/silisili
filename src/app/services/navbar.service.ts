import { Injectable } from '@angular/core'
import { Router, RouterLink } from '@angular/router';

@Injectable()
export class NavbarService{
    visible: boolean

    constructor() {
        this.visible = true
    }

    hide() {
        this.visible = false
    }

    show() {
        this.visible = true
    }

}