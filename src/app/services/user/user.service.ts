import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { employee } from "../../mock/model";
import { map } from "rxjs/operators";

@Injectable()

export class EmployeeService{
    constructor(private http:HttpClient){ }

    employee: any;
    findEmp(empId: number): Observable<employee[]>{
        return this.http.get('localhost:4000/graphiql', {
            params: new HttpParams()
                .set('empID', empId.toString())
        }).pipe(map(res => res["employee"]))
    }
}