import { Injectable } from '@angular/core'
import {BehaviorSubject } from 'rxjs'

@Injectable()

export class TimeService {
    private timeIn = new BehaviorSubject(true)
    currentMsg = this.timeIn.asObservable()

    constructor() { }

    changeAttendance(message: boolean) {
        this.timeIn.next(true)
    }
}