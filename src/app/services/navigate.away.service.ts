import { Injectable } from '@angular/core'
import { CanDeactivate } from '@angular/router'
import { LoginPageComponent } from './../login-page/login-page.component'
import { NavbarService } from './navbar.service'

@Injectable()
export class NavigateAwayService implements CanDeactivate <LoginPageComponent> {

    constructor(public nav : NavbarService){}

    canDeactivate(target: LoginPageComponent){
        this.nav.show()
        return true
    }
}