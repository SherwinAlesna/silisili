import { Component, OnInit } from '@angular/core';
import { Time } from '@angular/common';

export interface Shift {
  value: String
  viewValue: String
}

@Component({
  selector: 'app-emp-shift',
  templateUrl: './emp-shift.component.html',
  styleUrls: ['./emp-shift.component.scss']
})


export class EmpShiftComponent implements OnInit {

  shifts: Shift[] = [
    {value: 'flexi', viewValue: 'Flexi-10'},
    {value: 'regular', viewValue: 'Regular'},
  ]
  constructor() { }

  ngOnInit() {

  }

}
