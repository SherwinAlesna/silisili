import { Component, OnInit, TemplateRef } from '@angular/core'
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material'
import { RegisterComponentComponent } from '../register-component/register-component.component'
import { ChangeShift, AddShift } from '../../dialog/dialog'
import { ManageEmployee } from '../../dialog/dialog-manageEmp'
import { TimeStatus } from '../../dialog/dialog-time-status'
import { Apollo } from 'apollo-angular'
import { Observable } from 'rxjs'
import { Query, employee } from '../../mock/model'
import { map } from 'rxjs/operators'
import { GetAllEmp, UpdateStatus, AddNewShift, UpdateEmp } from '../../apollo'
var dateFormat = require('dateformat')

@Component({
  selector: 'app-emp-management',
  templateUrl: './emp-management.component.html',
  styleUrls: ['./emp-management.component.scss']
})
export class EmpManagementComponent implements OnInit {
  
  dialogResult = ""
  modalRef: any;
  dialogRef: any
  datas: Observable<employee[]>
  isDisabled: boolean = false
  empData: any

  constructor(
    private dialog: MatDialog,
    private apollo: Apollo
  ) { }

  ngOnInit() {
    this.apollo.watchQuery<Query>({
      query: GetAllEmp
    }).valueChanges.subscribe(emps => {
      this.empData = emps.data.employees
    })
  }
  registerModal(template: TemplateRef<any>) {
    this.modalRef = this.dialog.open(template)
  }

  changeShift(id,identification_number, position, shift, status) {
    let dConfig = new MatDialogConfig()
    dConfig.data = {
      title: 'Shift',
      message: 'change shift',
      type: 'edit shift'
    }
    this.dialog.open(ChangeShift, dConfig).afterClosed().subscribe(res => {
      this.onConfirmDialog(res, id, identification_number, position, shift, status)
    })
  }
  addShift(id,identification_number, position, shift, status){
    let dConfig = new MatDialogConfig()
    dConfig.data = {
      title: 'Add Shift',
      message: 'Add Shift',
      type: 'add shift'
    }
    this.dialog.open(AddShift, dConfig).afterClosed().subscribe(res => {
      this.onAddShift(res, id, identification_number, position, shift, status)
    })
  }

  onConfirmDialog(val,id,identification_number, position, shift, status) {
    this.updateStatus(val, id, identification_number, position, shift, status)
  }
  changeStatus(id, identification_number, position, shift, status){
    let dConfig = new MatDialogConfig()
    dConfig.data = {
      title: 'Status',
      message: status,
      type: 'status'
    }
    this.dialog.open(TimeStatus, dConfig).afterClosed().subscribe(res => {
      this.onConfirmDialog(res, id,identification_number, position, shift, status)
    })
  }
  updateStatus(val,id,identification_number, position, shift, status) {
    val == 'status' ? this.onUpdateStatus(id, identification_number, position, shift, status) :
      this.onChangeShift(id, identification_number, position, val, status)
  }
  onUpdateStatus(id,identification_number, position, shift, status) {
    this.apollo.mutate({
      mutation: UpdateStatus,
      variables: {
        id,
        emp: {
          identification_number,
          position,
          shift,
          status: this.onChangeStatus(status)
        }
      }
    }).subscribe(() => {}, err => {})
  }
  onChangeStatus(status) {
    let stats: string
    if(status == 'Activate') {
      stats = 'Deactivate'
    }
    else if(status == 'Deactivate') {
      stats = 'Activate'
    }
    return stats
  }

  onChangeShift(id,identification_number, position, shift, status) {
    this.apollo.mutate({
      mutation: UpdateStatus,
      variables: {
        id,
        emp: {
          identification_number,
          position,
          shift,
          status
        }
      }
    }).subscribe(() => {}, err => {})
  }
  onAddShift(res, id,identification_number, position, shift, status) {
    this.apollo.mutate({
      mutation: AddNewShift,
      variables: {
        id,
        emp_info: {
          identification_number,
          position,
          shift: 'Custom_Shift',
          status
        },
        shifts: {
          shiftDateFrom: res[0].DateFrom,
          shiftDateTo: res[0].DateTo,
          shiftTimeStart: `${res[0].TimeStart} ${res[0].fromPeriod}`,
          shiftTimeEnd: `${res[0].TimeEnd} ${res[0].toPeriod}`
        }
      }
    }).subscribe(() => {}, err => {})
  }
  modifyEmp(id,fN,mN,lN,bday,mobileNumber,email,home,des,team,dep,supervisor,idNumber,pos,shift,status) {
    let dConfig = new MatDialogConfig()
    dConfig.data = {
      title: 'Modify Employee',
      message: 'Manage Employee',
      type: 'editEmp',
      id,
      firstName: fN,
      middleName: mN,
      lastName: lN,
      bday: bday,
      mobile: mobileNumber,
      email: email,
      home,
      designation: des,
      team,
      department: dep,
      supervisor,
      identificatonNumber: idNumber,
      position: pos,
      shift,
      status
    }
    this.dialog.open(ManageEmployee,dConfig).afterClosed().subscribe(res => {
      this.onEditEmp(res[0])
    })
  }
  onEditEmp(data) {
    this.apollo.mutate({
      mutation: UpdateEmp,
      variables: {
        id: data.id,
        person: {
          first_name: data.firstName,
          middle_name: data.middleName,
          last_name: data.lastName,
          birthday: data.birthday,
          mobile_number: data.mobileNumber,
          email: data.email,
          home_address: data.homeAddress
        },
        work: {
          designation: data.designation,
          team: data.team,
          department: data.department,
          supervisor: data.supervisor
        },
        emp: {
          identification_number: String(data.identificationNumber),
          position: data.position,
          shift: data.shift,
          status: data.status
        }
      }
    }).subscribe(edit => {}, err => {})
  }
  ngOnDestroy() {
    this.dialogResult
    this.modalRef
    this.dialogRef
  }
}
