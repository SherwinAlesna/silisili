import { Component, OnInit, Directive } from '@angular/core'
import { FormGroup, FormControl, FormBuilder } from '@angular/forms'
import { Apollo } from 'apollo-angular'
import { AuthService, GoogleLoginProvider } from "angularx-social-login"
import { MatSnackBar } from '@angular/material'
let dateFormat = require('dateformat')
import { MatDialog } from '@angular/material/dialog'
import { attendance, Query, employee } from '../../mock/model'
import { CreateEmp, GetAllEmp } from '../../apollo'
import { Sample, SupervisorEnum } from '../../../../server/db/graphql/types' // add supervisor enum data how??

@Component({
  selector: 'app-register-component',
  templateUrl: './register-component.component.html',
  styleUrls: ['./register-component.component.scss']
})
export class RegisterComponentComponent implements OnInit {
  isLinear = true
  private employeeDetailGroup: FormGroup
  private designationFormGroup: FormGroup
  private thirdFormGroup: FormGroup
  private loginFormGroup: FormGroup
  private currId: String
  private getId: String
  private bday: Date
  private error =  {
    email: ''
  }
  
  constructor(
    private apollo: Apollo,
    private authService: AuthService,
    private modalRef: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  // WIP: get data from database later 
  private designations: any = [
    {value: 'Employee', viewValue: 'Employee'},
    {value: 'Supervisor', viewValue: 'Supervisor'},
    {value: 'HR', viewValue: 'HR'},
    {value: 'Manager', viewValue: 'Manager'},
  ]
  private teams: any = [
    {value: 'AmihanUnawa', viewValue: 'AMIHAN Unawa'},
    {value: 'AmihanCloudBlocks', viewValue: 'AMIHAN Cloud Blocks'},
    {value: 'AmihanNimble', viewValue: 'AMIHAN Nimble'}
  ]
  private supervisors: any = [
    {value: 'Renz', viewValue: 'Renz Bernados'},
    {value: 'Rendz', viewValue: 'Rendz Alerta'},
    {value: 'Press', viewValue: 'Press Button'},
    {value: 'Daniel', viewValue: 'Daniel Ubanan'},
    {value: 'Sherwin', viewValue: 'Sherwin Alesna'}
  ]
  private departments: any = [
    {value: 'ProductAndInnovation', viewValue: 'Product and Innovation'},
    {value: 'ProductTeam', viewValue: 'Product Team'},
    {value: 'ManagedServices', viewValue: 'Managed Services'},
    {value: 'TechOps', viewValue: 'Technical Operations'},
    {value: 'Consulting', viewValue: 'Consulting'}
  ]
  private positions: any = [
    {value: 'Probationary', viewValue: 'Probationary'},
    {value: 'Regular', viewValue: 'Regular'},
    {value: 'Consultant', viewValue: 'Consultant'},
  ]
  private shifts: any = [
    {value: 'Flexi_10', viewValue: 'Flexi-10'},
    {value: 'Full_Flexi', viewValue: 'Full-Flexi'},
    {value: 'Regular', viewValue: 'Regular'}
  ]
  private stats: any = [
    {value: 'Activate', viewValue: 'Activate'},
    {value: 'Deactivate', viewValue: 'Deactivate'}
  ]
  
  ngOnInit() {
    this.employeeDetailGroup = new FormGroup({
      empFName: new FormControl(),
      empMidInit: new FormControl(),
      empLName: new FormControl(),
      empBDay: new FormControl(),
      empAge: new FormControl(),
      empMobNum: new FormControl(),
      empEmail: new FormControl(),
      empHomeAdd: new FormControl()
    })
    this.designationFormGroup = new FormGroup({
      empTeam: new FormControl(),
      empDesignation: new FormControl(),
      empDept: new FormControl(),
      empSupervisor: new FormControl(),
    })
    this.thirdFormGroup = new FormGroup({
      IDNum: new FormControl(),
      empShift: new FormControl(),
      empPosition: new FormControl(),
      empShiftReal: new FormControl(),
      empStatus: new FormControl()
    })
    this.loginFormGroup = new FormGroup({
      empUserName: new FormControl(),
      empPassword: new FormControl()
    })
    this.addSupervisor()
  }
  addSupervisor() {
    let designation, firstName, lastName
    this.apollo.watchQuery<Query>({
      query: GetAllEmp
    }).valueChanges.subscribe(datas => {
      let emps = datas.data.employees
      for(let i of emps) {
        designation = i.work_details.designation
        firstName = i.person_details.first_name
        lastName = i.person_details.last_name
      }
      if(designation == 'Supervisor') {
        this.supervisors.push({
          value: `${firstName}`,
          viewValue: `${firstName} ${lastName}`
        })
      }
      let lastSupervisor = this.supervisors[this.supervisors.length -1]
      
    })
  }
  onSubmit(first_name,middle_name,last_name,birthday,mobile_number,email,home_address,
    designation,team,department,supervisor,identification_number,position,shift,username,password
  ){
    let now = new Date()
    this.bday = dateFormat(birthday, 'mm-dd-yyyy')
    let shiftNow = dateFormat(now, 'dddd mm-dd-yyyy')
    let sixDays = dateFormat(new Date(now.getFullYear(),now.getMonth(),now.getDate() - now.getDay() + 9), 'dddd mm-dd-yyyy')
    identification_number = String(identification_number)
    this.apollo.mutate({
       mutation: CreateEmp,
       variables: {
         id: this.getId,
         person: {
           first_name,
           middle_name,
           last_name,
           birthday: this.bday,
           mobile_number,
           email,
           home_address
         },
         work: {
           designation,
           team,
           department,
           supervisor
         },
         employee: {
           identification_number,
           position,
           shift: 'Regular',
           status: 'Activate'
         },
         shifts: {
           shiftDateFrom: shiftNow,
           shiftDateTo: sixDays,
           shiftTimeStart: '10:00 AM',
           shiftTimeEnd: '07:00 PM'
         },
         loginCred: {
           username,
           password
         }
       }
    }).subscribe(() => {
      // location.reload()
    }, err => {})
  }
  // get email id
  checkEmail(mail) {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(data => {
      this.getId = data.id.toString()
      data.email != mail ? this.error.email = 'Email did not match' : this.error.email = ''
      return this.error
    })
  }
  handleTextChange(e) {
    let name = e.srcElement.name
    let value = e.srcElement.value
    this.validate(name, value)
  }
  validate(k, v) {
    switch (k) {
      case 'email':
        v.length < 4 ? this.error.email = 'too short' : this.error.email = ''
        // validate if email existed later
        break;
      default:
        break;
    }
  }
  ngOnDestroy() {
    
  }
}
