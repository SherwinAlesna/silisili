import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router'
import { AuthService, GoogleLoginProvider, SocialUser } from "angularx-social-login"
import { CookieService } from 'ngx-cookie-service'
import { Apollo } from 'apollo-angular'
import gql from 'graphql-tag'
import { Query } from '../mock/model'
import { NavbarService } from '../services/navbar.service'
import { LogData } from '../services/login.service'
import { GetEmployee, GetAllEmp } from '../apollo'

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  private currID: String
  private currEmail: String
  private showNav: Boolean
  private user: any
  private admin = {
    "person_details": {
      "first_name": "admin",
      "middle_name": "a",
      "last_name": "admin",
      "birthday": "10/31/31",
      "mobile_number": "21321321",
      "email": "sample@gmail.com",
      "home_address": "sampleAddress"
    },
    "work_details":  {
      "designation": "Manager",
      "team": "AmihanNimble",
      "department": "TechOps",
      "supervisor": "Sherwin"
    },
    "employee_information": {
      "identification_number": "237183721",
      "position": "Probationary",
      "shift": "Flexi_10",
      "status": "Activate"
    },
    "shifts": {
      "shiftDateFrom": "Wednesday, 01-09-2019",
      "shiftDateTo": "Thursday, 01-10-2019",
      "shiftTimeStart": "10:00 AM",
      "shiftTimeEnd": "07:00 PM"
    },
    "loginCred": {
      "username": "admin",
      "password": "admin"
    }
  }

  constructor(
    private datas: LogData,
    public nav: NavbarService,
    private authService: AuthService,
    public router: Router,
    private cookieService: CookieService,
    private apollo: Apollo
  ) { }

  ngOnInit() {
    this.nav.show()
  }
  googleLogin() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(user => {
      this.currID = user.id.toString()
      this.currEmail = user.email.toString()

      // check if ID exists in databse
      this.apollo.watchQuery<Query>({
        query: GetEmployee,
        variables: {
          id: this.currID
        }
      }).valueChanges.subscribe( data  => {
        if(data.data.employee.id == this.currID){
          this.localStorages(data.data.employee.work_details.designation,user.name,user.email,user,this.currID)
        }else{
          this.router.navigate(["401"])
        }
      }, err => {
        this.router.navigate(["401"])
      })
    })
  }
  inputLogin(username,password) {
    this.apollo.watchQuery<Query>({
      query: GetAllEmp
    }).valueChanges.subscribe(allEmps => {
      let emps = allEmps.data.employees
      for(let v of emps) {
        let uName = v.loginCred.username
        let pass = v.loginCred.password
        if(username == uName && password == pass) {
          if(username == 'admin' && password == 'admin') {
            this.localStorages('Manager','admin','admin@gmail.com',this.admin,'1')
          }
          else {
            // get data from diff user later
            this.localStorages(v.work_details.designation,v.person_details.first_name,v.person_details.email,v,v.id)
          }
        }
      }
    })
  }
  localStorages(role, name, email, profile, id) {
    localStorage.setItem('ID', id)
    localStorage.setItem('role', role)
    localStorage.setItem('name', name)
    localStorage.setItem('email', email)
    localStorage.setItem('TI', null)
    sessionStorage.setItem('profile', JSON.stringify(profile))
    this.cookieService.set('profile', JSON.stringify(profile))
    this.datas.addData(profile)
    this.router.navigate(['home'])
  }
}
