import { Injectable } from '@angular/core'
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { CookieService } from 'ngx-cookie-service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private cookieStorage: CookieService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.cookieStorage.get('profile')) {
            this.router.navigate(['/home'])
            return true
        } else {
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }})
            return false
        }   
    }
}
    