import { Component, OnInit, Inject } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { MatSnackBar } from '@angular/material'
import { EmpManagementComponent } from '../manage/emp-management/emp-management.component'
var dateFormat = require('dateformat')

// change shift dialog
@Component({templateUrl: 'dialog-shift.html'})
export class ChangeShift {

  title: string
  message: string
  onConfirmCB: string
  onCloseCB: string = 'close'

  constructor(
    private dialogRef: MatDialogRef<EmpManagementComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private snackBar: MatSnackBar
  ) {
    this.title = data.title
    this.message = data.message
    this.onConfirmCB = data.type
  }

  private shifts: any = [
    {value: 'Flexi_10', viewValue: 'Flexi-10'},
    {value: 'Full_Flexi', viewValue: 'Full-Flexi'},
    {value: 'Custom_Shift', viewValue: 'Custom Shift'},
    {value: 'Regular', viewValue: 'Regular'}
  ]

  onConfirm(shift) {
    this.dialogRef.close(shift)
    this.snackBar.open('shift updated', '', {
      duration: 4000
    })
  }
  onClose() {
    this.dialogRef.close(this.onCloseCB)
  }

  ngDestroy() {
    this.dialogRef
  }
}

// add shift dialog
@Component({templateUrl: 'dialog-addshift.html'})
export class AddShift {

  title: string
  message: string
  onConfirmCB: string
  onCloseCB: string = 'close'
  valueTimeTo: any
  time: any
  fromPeriod: string = 'AM'
  toPeriod: string = 'AM'
  periods: any = [
    {value: 'AM', viewValue: 'AM'},
    {value: 'PM', viewValue: 'PM'}
  ]

  constructor(
    private dialogRef: MatDialogRef<EmpManagementComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {
    this.title = data.title
    this.message = data.message
    this.onConfirmCB = data.type
  }

  handleTextChange(e) {
    let name = e.srcElement.name
    let value = e.srcElement.value
    if(name == 'timeFrom') {
      value < 16 ? this.valueTimeTo = parseInt(value) + 9 : this.valueTimeTo = ''
    }
  }
  timeRange(timeTo,shiftTimeEnd) {
    if(timeTo == ''){
      this.time = shiftTimeEnd
    } 
    else {
      this.time = this.valueTimeTo
    }
    return this.time.toString()
  }
  
  onConfirm(shiftDateFrom,shiftDateTo,shiftTimeStart,shiftTimeEnd,fromPeriod,toPeriod) {
    let DateFrom = dateFormat(shiftDateFrom, 'dddd, mm-dd-yyyy')
    let DateTo = dateFormat(shiftDateTo, 'dddd, mm-dd-yyyy')
    let results = [
      {
        DateFrom,
        DateTo,
        TimeStart: shiftTimeStart,
        TimeEnd: this.timeRange(this.valueTimeTo, shiftTimeEnd),
        fromPeriod,
        toPeriod
      }
    ]
    this.dialogRef.close(results)
  }
  onClose() {
    this.dialogRef.close(this.onCloseCB)
  }

  ngDestroy() {
    this.dialogRef
  }
}