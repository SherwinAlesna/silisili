import { Component, Inject } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { AttendanceViewComponent } from '../attendance-view/attendance-view.component'

@Component({templateUrl: 'dialog-clockin.html'})
export class ChangeTime {

  onConfirmCB: string
  onCloseCB: string = 'close'
  message: string
  title: string
  selected: string
  pers: string

  times: any = [
    {value: '00:00'},
    {value: '00:30'},
    {value: '01:00'},
    {value: '01:30'},
    {value: '02:00'},
    {value: '02:30'},
    {value: '03:00'},
    {value: '03:30'},
    {value: '04:00'},
    {value: '04:30'},
    {value: '05:00'},
    {value: '05:30'},
    {value: '06:00'},
    {value: '06:30'},
    {value: '07:00'},
    {value: '07:30'},
    {value: '08:00'},
    {value: '08:30'},
    {value: '09:00'},
    {value: '09:30'},
    {value: '10:00'},
    {value: '10:30'},
    {value: '11:00'},
    {value: '11:30'},
    {value: '12:00'},
  ]
  period: any = [
    {value: 'AM'},
    {value: 'PM'}
  ]


  constructor(
  	private dialogRef: MatDialogRef<AttendanceViewComponent>,
  	@Inject(MAT_DIALOG_DATA) private data: any
  	) {
  	this.message = data.message
  	this.title = data.title
  	this.onConfirmCB = data.type
    this.pers = 'AM'
  }

  onConfirm(time, period) {
    let results = [
      {
        type: this.onConfirmCB,
        time,
        period
      }
    ]
  	this.dialogRef.close(results)
  }

  onClose() {
  	this.dialogRef.close(this.onCloseCB)
  }
}