import { Component, Inject } from '@angular/core'
import { MatDialogRef } from '@angular/material'
import { AttendanceViewComponent } from '../attendance-view/attendance-view.component'

@Component({
  templateUrl: 'dialog-notify.html'
})
export class Notify {

  constructor(
  	private dialogRef: MatDialogRef<AttendanceViewComponent>
  ) {}

  onClose() {
  	this.dialogRef.close()
  }
}