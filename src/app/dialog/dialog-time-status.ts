import { Component, Inject, OnInit } from '@angular/core'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { MainFrameComponent } from '../main-frame/main-frame.component'

@Component({ templateUrl: 'dialog-time-status.html'})
export class TimeStatus {
  
  title: string
  message: string
  onConfirmCB: string
  onCloseCB: string = 'close'
  clockin: string
  clockout: string
  newClockin: string
  newClockout: string

  constructor(
  	private dialogRef: MatDialogRef<MainFrameComponent>,
  	@Inject(MAT_DIALOG_DATA) private data: any
  ) { 
  	this.title = this.data.title
  	this.message = this.data.message
  	this.onConfirmCB = this.data.type
  }

  ngOnInit() {
    this.changeStatus(this.message)
    if(this.onConfirmCB == 'newTime') {
      this.clockin = this.data.clockin
      this.clockout = this.data.clockout
      this.newClockin = this.data.newClockin
      this.newClockout = this.data.newClockout
    }
  }
  changeStatus(val) {
    if(val == 'Activate') {
      this.message = 'Deactivate'
    }
    else if(val == 'Deactivate') {
      this.message = 'Activate'
    }
    return this.message
  }
  onConfirm() {
  	this.dialogRef.close(this.onConfirmCB)
  }
  reject() {
    this.dialogRef.close('rejected')
  }
  onClose() {
  	this.dialogRef.close(this.onCloseCB)
  }
}
