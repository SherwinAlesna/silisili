import { Component, Inject, OnInit } from '@angular/core'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { FormGroup, FormControl } from '@angular/forms'
import { EmpManagementComponent } from '../manage/emp-management/emp-management.component'
var dateFormat = require('dateformat')

@Component({
  templateUrl: 'dialog-manageEmp.html',
  styleUrls: ['dialog-manageEmp.scss']
})
export class ManageEmployee implements OnInit {
  
  onConfirmCB: string = 'confirm'
  onCloseCB: string = 'close'
  message: string
  title: string
  id: string
  firstName: string
  middleName: string
  lastName: string
  birthday: string
  mobileNumber: string
  email: string
  homeAddress: string
  designation: string
  team: string
  department: string
  supervisor: string
  identificationNumber: string
  position: string
  shift: string
  status: string
  employeeDetailGroup: FormGroup
  designationFormGroup: FormGroup
  finalFormGroup: FormGroup
  designations: any = [
    {value: 'Employee', viewValue: 'Employee'},
    {value: 'Supervisor', viewValue: 'Supervisor'},
    {value: 'HR', viewValue: 'HR'},
    {value: 'Manager', viewValue: 'Manager'},
  ]
  teams: any = [
    {value: 'AmihanUnawa', viewValue: 'AMIHAN Unawa'},
    {value: 'AmihanCloudBlocks', viewValue: 'AMIHAN Cloud Blocks'},
    {value: 'AmihanNimble', viewValue: 'AMIHAN Nimble'}
  ]
  departments: any = [
    {value: 'ProductAndInnovation', viewValue: 'Product and Innovation'},
    {value: 'ProductTeam', viewValue: 'Product Team'},
    {value: 'ManagedServices', viewValue: 'Managed Services'},
    {value: 'TechOps', viewValue: 'Technical Operations'},
    {value: 'Consulting', viewValue: 'Consulting'}
  ]
  supervisors: any = [
    {value: 'Renz', viewValue: 'Renz Bernados'},
    {value: 'Rendz', viewValue: 'Rendz Alerta'},
    {value: 'Press', viewValue: 'Press Button'},
    {value: 'Daniel', viewValue: 'Daniel Ubanan'},
    {value: 'Sherwin', viewValue: 'Sherwin ABCD'}
  ]
  positions: any = [
    {value: 'Probationary', viewValue: 'Probationary'},
    {value: 'Regular', viewValue: 'Regular'},
    {value: 'Consultant', viewValue: 'Consultant'},
  ]

  constructor(
  	private dialogRef: MatDialogRef<EmpManagementComponent>,
  	@Inject(MAT_DIALOG_DATA) private data: any
  ) {
  	this.title = data.title
  	this.message = data.message
  	this.id = data.id
  	this.firstName = data.firstName
  	this.middleName = data.middleName
  	this.lastName = data.lastName
  	this.birthday = dateFormat(data.bday,'mm-dd-yyyy')
  	this.mobileNumber = data.mobile
  	this.email = data.email
  	this.homeAddress = data.home
  	this.designation = data.designation
  	this.team = data.team
  	this.department = data.department
  	this.supervisor = data.supervisor
  	this.identificationNumber = data.identificationNumber
  	this.position = data.position
  	this.shift = data.shift
  	this.status = data.status
  }
  ngOnInit() {
    this.employeeDetailGroup = new FormGroup({
      empFName: new FormControl(),
      empMidInit: new FormControl(),
      empLName: new FormControl(),
      empBDay: new FormControl(),
      empMobNum: new FormControl(),
      empEmail: new FormControl(),
      empHomeAdd: new FormControl()
    })
    this.designationFormGroup = new FormGroup({
      empTeam: new FormControl(),
      empDesignation: new FormControl(),
      empDept: new FormControl(),
      empSupervisor: new FormControl(),
    })
    this.finalFormGroup = new FormGroup({
      IDNum: new FormControl(),
      empPosition: new FormControl()
    })
  }

  onConfirm(first_name,middle_name,last_name,birthday,mobile_number,email,home_address,designation,team,department,supervisor,identification_number,position) {
  	let bday = dateFormat(birthday, 'mm-dd-yyyy')
  	let res = [{
  	  id: this.id,
  	  firstName: first_name,
  	  middleName: middle_name,
  	  lastName: last_name,
  	  birthday: bday,
  	  mobileNumber: mobile_number,
  	  email,
  	  homeAddress: home_address,
  	  designation,
  	  team,
  	  department,
  	  supervisor,
  	  identificationNumber: identification_number,
  	  position,
  	  shift: this.shift,
  	  status: this.status
  	}]
  	this.dialogRef.close(res)
  }
  onClose() {
  	this.dialogRef.close(this.onCloseCB)
  }
}