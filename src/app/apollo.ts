import gql from 'graphql-tag'

export const GetAllEmp = gql`
  query employees {
    employees {
      id
      person_details {
        first_name
        middle_name
        last_name
        birthday
        mobile_number
        email
        home_address
      }
      work_details {
        designation
        team
        department
        supervisor
      }
      employee_information {
        identification_number
        position
        shift
        status
      }
      shifts {
        shiftDateFrom
        shiftDateTo
        shiftTimeStart
        shiftTimeEnd
      }
      loginCred {
        username
        password
      }
    }
  }
`
export const Clockin = gql`
  mutation attClockin($id: String!, $attId: String!, $emp: InputPersonDet!, $date: String!, $in: String!, $out: String!, $app: Approved!, $newClockin: String!, $newClockout: String!, $role: Designation!, $supervisor: Supervisor!, $late: Boolean!) {
    attClockin(id: $id, attId: $attId, employee: $emp, date: $date, clockin: $in, clockout: $out, approved: $app, newClockin: $newClockin, newClockout: $newClockout, role: $role, supervisor: $supervisor, late: $late) {
      id
      attId
      employee {
        first_name
        middle_name
        last_name
        birthday
        mobile_number
        email
        home_address
      }
      date
      clockin
      clockout
      approved
      newClockin
      newClockout
      
      role
      supervisor
      late
    }
  }
`
export const Clockout = gql`
  mutation attClockout($id:String!,$out:String!,$app:Approved!, $underTime: Boolean!) {
    attClockout(id:$id,clockout:$out,approved:$app,underTime:$underTime) {
      id
      clockout
      approved
      underTime
    }
  }
`
export const NewClockin = gql`
  mutation newClockin($attId:String,!$in:String!,$newClockin:String!,$app:Approved!) {
    newClockin(attId:$attId,clockin:$in,newClockin:$newClockin,approved:$app) {
      attId
      clockin
      newClockin
      approved
    }
  }
`
export const NewClockout = gql`
  mutation newClockout($attId:String!,$out:String!,$newClockout:String!,$app:Approved!) {
    newClockout(attId:$attId,clockout:$out,newClockout:$newClockout,approved:$app) {
      attId
      clockout
      newClockout
      approved
    }
  }
`
export const UpdateTime = gql`
  mutation upadteTime($attId:String!,$in:String!,$out:String!,$newClockin:String!,$newClockout:String!,$app:Approved!) {
    updateTime(attId:$attId,clockin:$in,clockout:$out,newClockin:$newClockin,newClockout:$newClockout,approved:$app) {
      attId
      clockin
      clockout
      newClockin
      newClockout
      approved
    }
  }
`
export const RejectTime = gql`
  mutation rejectTime($attId:String!,$newClockin:String!,$newClockout:String!,$app:Approved!) {
    rejectAttendance(attId:$attId,newClockin:$newClockin,newClockout:$newClockout,approved:$app) {
      attId
      newClockin
      newClockout
      approved
    }
  }
`
export const UpdateAllTime = gql`
  mutation updateAllTime($id:String,$newClockin:String,$newClockout:String,$app:Approved) {
    updateAllTime(id:$id,newClockin:$newClockin,newClockout:$newClockout,approved:$app) {
      id
      newClockin
      newClockout
      approved
    }
  }
`
export const ApproveAllTime = gql`
  mutation approvedAllTime($attId:String!,$in:String!,$out:String!,$app:Approved) {
    approvedAllTime(attId:$attId,clockin:$in,clockout:$out,approved:$app) {
      attId
      clockin
      clockout
      approved
    }
  }
`
export const DefaultShift = gql`
  mutation addShift($id:String!, $emp_info:InputEmployeeInformation!, $shifts:InputShift!){
    addShift(id:$id, employee_information:$emp_info, shifts:$shifts) {
      id
      employee_information {
        identification_number
        position
        shift
        status
      }
      shifts {
        shiftDateFrom
        shiftDateTo
        shiftTimeStart
        shiftTimeEnd
      }
    }
  }
`
export const CreateEmp = gql`
  mutation addEmp($id:String!,$person:InputPersonDet!,$work:InputWorkDet!,$employee:InputEmployeeInformation!,$shifts:InputShift!,$loginCred:InputLoginCred!) {
    addEmployee(id: $id,person_details: $person,work_details: $work,employee_information: $employee, shifts:$shifts,loginCred:$loginCred) {
      id
      person_details {
        first_name
        middle_name
        last_name
        birthday
        mobile_number
        email
        home_address
      }
      work_details {
        designation
        team
        department
        supervisor
      }
      employee_information {
        identification_number
        position
        shift
        status
      }
      shifts {
        shiftDateFrom
        shiftDateTo
        shiftTimeStart
        shiftTimeEnd
      }
      loginCred {
        username
        password
      }
    }
  }
`
export const UpdateEmp = gql`
  mutation updateEmp($id:String!,$person:InputPersonDet!,$work:InputWorkDet!,$emp:InputEmployeeInformation!) {
    updateEmp(id:$id,person_details:$person,work_details:$work,employee_information:$emp) {
      id
      person_details {
        first_name
        middle_name
        last_name
        birthday
        mobile_number
        email
        home_address
      }
      work_details {
        designation
        team
        department
        supervisor
      }
      employee_information {
        identification_number
        position
        shift
        status
      }
    }
  }
`
export const UpdateStatus = gql`
  mutation changeStatus($id:String!, $emp:InputEmployeeInformation!) {
    changeStatus(id:$id, employee_information:$emp) {
      id
      employee_information {
        identification_number
        position
        shift
        status
      }
    }
  }
`
export const AddNewShift = gql`
  mutation addShift($id:String!, $shifts:InputShift!, $emp_info:InputEmployeeInformation!) {
    addShift(id:$id, shifts:$shifts, employee_information:$emp_info) {
      id
      employee_information {
        identification_number
        position
        shift
        status
      }
      shifts {
        shiftDateFrom
        shiftDateTo
        shiftTimeStart
        shiftTimeEnd
      }
    }
  }
`
export const GetEmployee = gql`
  query employee($id: String!){
    employee(id:$id) {
      id
      person_details {
        first_name
        middle_name
        last_name
        birthday
        mobile_number
        email
        home_address
      }
      work_details {
        designation
        team
        department
        supervisor
      }
      employee_information {
        identification_number
        position
        shift
        status
      }
      shifts {
        shiftDateFrom
        shiftDateTo
        shiftTimeStart
        shiftTimeEnd
      }
    }
  }
`
export const GetAllAttendances = gql`
  query getAllAtt {
    allAttendance {
      attId
      employee {
        first_name
        middle_name
        last_name
        birthday
        mobile_number
        email
        home_address
      }
      date
      clockin
      clockout
      approved
      newClockin
      newClockout
      role
      supervisor
      late
      underTime
    }
  }
`
export const GetEmpAttendance = gql`
  query getAtt($id:String!) {
    attendance(id:$id) {
      attId
      employee {
        first_name
        middle_name
        last_name
        birthday
        mobile_number
        email
        home_address
      }
      date
      clockin
      clockout
      approved
      newClockin
      newClockout
      role
      supervisor
      late
      underTime
    }
  }
`
