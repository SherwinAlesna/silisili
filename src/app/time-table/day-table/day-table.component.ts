import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-day-table',
  templateUrl: './day-table.component.html',
  styleUrls: ['./day-table.component.scss']
})
export class DayTableComponent implements OnInit {
  today = new Date()
  monOfWeek = new Date(this.today.getFullYear(),this.today.getMonth(),this.today.getDate() - this.today.getDay()+ 1)
  nextDay : any
  private dateHeader: any
  dailyWork: any
  constructor() { }

  ngOnInit() {
    this.nextDay = new Date(this.monOfWeek)
    this.dateHeader = [ 
      {"day":"Monday","date": this.monOfWeek},
      {"day":"Tuesday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)},
      {"day":"Wednesday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)},
      {"day":"Thursday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)},
      {"day":"Friday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)},
      {"day":"Saturday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)},
      {"day":"Sunday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)}
    ]
    this.initDailyWork()
  }

  initDailyWork(){
    this.dailyWork = [
        {
          "id": 1, "Act": "Babysitting", "Client": "BDO", "Project": "Wallet", "hours": 0
        },
        {
          "id": 2, "Act": "CodeCrying", "Client": "SunLife", "Project": "ATM-W", "hours": 4.00
        },
        {
          "id": 3, "Act": "Project Adversinging", "Client": "Client`e", "Project": "Proyekto", "hours": 5
        }
    ]
  }
}
