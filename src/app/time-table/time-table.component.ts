import { Component, OnInit } from '@angular/core'
import { Apollo } from 'apollo-angular'

import gql from 'graphql-tag'

import { Observable, Subscription } from 'rxjs'
import { map } from 'rxjs/operators'
import { AnimationKeyframesSequenceMetadata } from '@angular/animations';
import { yearsPerPage } from '@angular/material/datepicker/typings/multi-year-view';


const CurrentUserForTime = gql`
    query allWeek {
      allWeekData {
        id 
        Act
        Mon
        Tue                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        Wed
        Thu
        Fri
        Sat
        Sun        
      }
    }
    `

@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.scss']
})
export class TimeTableComponent implements OnInit {

  constructor() { }
  ngOnInit() { }

}