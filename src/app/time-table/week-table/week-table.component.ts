import { Component, OnInit } from '@angular/core'
import { Apollo } from 'apollo-angular'
import gql from 'graphql-tag'
import { employee, Query } from '../../mock/model'
import { Observable, Subscription } from 'rxjs'

const gglEmployee = gql`
  query employees {
    employees {
      id
      person_details {
        first_name middle_name last_name birthday mobile_number email home_address
      }
      work_details {
        designation team department supervisor
      }
      employee_information {
        identification_number position shift status
      }
      shifts {
        shiftDateFrom shiftDateTo shiftTimeStart shiftTimeEnd
      }
    }
  }
`
const UpdateTimeforUser = gql`
    mutation valueChange($nData: newData!) {
      changeValue(changeWeek: $nData) {
        id
        Act
        Mon
        Tue
        Wed
        Thu
        Fri
        Sat
        Sun
      }
    }
    `

const addTimeforUser = gql`
  mutation newValue($newData: newRowData!) {
    createRow(newRow: $newData) {
      id
      Act
      Mon
      Tue
      Wed
      Thu
      Fri
      Sat
      Sun
    }
  }
  `
   
@Component({
  selector: 'app-week-table',
  templateUrl: './week-table.component.html',
  styleUrls: [
    './week-table.component.scss'
  ]
})
export class WeekTableComponent implements OnInit {

  today = new Date()
  monOfWeek = new Date(this.today.getFullYear(),this.today.getMonth(),this.today.getDate() - this.today.getDay()+ 1)
  nextDay : any
  private dateHeader: any
  weekValue: any
  perProjectTotal: any
  perDayTotal: any
  intersectTotal: any
  days: any

  constructor(private apollo: Apollo) { }

  ngOnInit() {
    this.queryEmployee()
    this.nextDay = new Date(this.monOfWeek)

    this.dateHeader = [ 
      {"day":"Monday","date": this.monOfWeek},
      {"day":"Tuesday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)},
      {"day":"Wednesday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)},
      {"day":"Thursday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)},
      {"day":"Friday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)},
      {"day":"Saturday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)},
      {"day":"Sunday","date": this.nextDay.setDate(this.nextDay.getDate() + 1)}
    ]

    this.weekValue = [  
      {Act: "Developing", Mon: 8, Tue: 7, Wed : 10, Thu: 0, Fri: 0, Sat: 4, Sun: 0},
      {Act: "Project Meeting", Mon: 4, Tue: 7, Wed : 7, Thu: 0, Fri: 0, Sat: 3.2, Sun: 0},
      {Act: "Design", Mon: 4, Tue: 2, Wed : 10, Thu: 2, Fri: 0, Sat: 0, Sun: 0},
      {Act: "Marketing", Mon: 6, Tue: 1, Wed : 10, Thu: 0, Fri: 0, Sat: 0, Sun: 0}
    ]

    this.perProjectTotal = []
    this.perDayTotal = [0, 0, 0, 0, 0, 0, 0, 0]
    this.intersectTotal = 0

    for(let i of this.weekValue){
      let temp = {sum: i.Mon + i.Tue + i.Wed + i.Thu + i.Fri + i.Sat + i.Sun}
      this.perProjectTotal.push(temp)     

      this.perDayTotal[0] += i.Mon
      this.perDayTotal[1] += i.Tue
      this.perDayTotal[2] += i.Wed
      this.perDayTotal[3] += i.Thu
      this.perDayTotal[4] += i.Fri
      this.perDayTotal[5] += i.Sat
      this.perDayTotal[6] += i.Sun
      this.perDayTotal[7] += temp.sum
    }

  }
  
  queryEmployee(){
    this.apollo.watchQuery<Query>({
      query: gglEmployee
    }).valueChanges.subscribe(result => {
      console.log(result.data.employees)
    })
  }

}
