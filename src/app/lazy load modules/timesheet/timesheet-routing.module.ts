import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { TimeTableComponent } from './../../time-table/time-table.component'
import { WeekTableComponent } from './../../time-table/week-table/week-table.component'
import { DayTableComponent } from './../../time-table/day-table/day-table.component'

const routes: Routes = [
    {
        path: "time-sheet",
        component: TimeTableComponent,
        children: [
            {
                path: "#weekly",
                component: WeekTableComponent
            },
            {
                path: "#daily",
                component: DayTableComponent
            }
        ]
    }

]

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)] 
})

export class TimesheetRoutingModule { }