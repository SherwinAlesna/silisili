import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { TimesheetRoutingModule } from "./timesheet-routing.module";
import { TimeTableComponent } from './../../time-table/time-table.component'
import { WeekTableComponent } from "../../time-table/week-table/week-table.component";
import { DayTableComponent } from "../../time-table/day-table/day-table.component";

import { MaterialModule } from "../../material.module";
import { FormsModule } from "@angular/forms";


@NgModule({
    imports: [CommonModule, TimesheetRoutingModule, MaterialModule, FormsModule, ],
    declarations: [
      TimeTableComponent, WeekTableComponent, DayTableComponent
    ]
  })
  export class TimesheetModule {}