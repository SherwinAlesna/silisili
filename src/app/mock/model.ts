type person_det = {
  first_name: String
  middle_name: String
  last_name: String
  birthday: String
  mobile_number: String
  email: String
  home_address: String
  work: work_det
}

type att_det = {
  first_name: String
  middle_name: String
  last_name: String
  birthdady: String
  mobile_number: String
  email: String
  home_address: String
}

type work_det = {
  designation: designation
  team: team
  department: department
  supervisor: supervisor
}

type emp_info = {
  identification_number: String
  position: String
  shift: String
  status: String
}

type shifts = {
  shiftDateFrom: String
  shiftDateTo: String
  shiftTimeStart: String
  shiftTimeEnd: String
}

enum designation {
  "Employee",
  "Supervisor",
  "HR",
  "Manager",
  "s"
}

enum team {
  "AmihanUnawa",
  "AmihanCloudBlocks",
  "AmihanNimble"
}

enum department {
  "ProductAndInnovation",
  "ProductTeam",
  "ManagedServices",
  "TechOps",
  "Consulting"
}

enum supervisor {
  "Sherwin",
  "Renz",
  "Rendz",
  "Press",
  "Daniel"
}

enum approved {
  "Approved",
  "Not_Approved",
  "For_Approved",
  "Reject"
}
enum attendanceStatus {
  "Late",
  "AWOL"
}

export type employee = {
  id: String
  person_details: person_det
  work_details: work_det
  employee_information: emp_info
  shifts: shifts
  loginCred: {
    username: String
    password: String
  }
}
export type attendance = {
  id: String,
  attId: String,
  employee: att_det,
  date: String,
  clockin: String,
  clockout: String,
  approved: String,
  newClockin: String,
  newClockout: String,
  status: attendanceStatus,
  role: designation,
  supervisor: supervisor
}
export type Query = {
  employees: employee[]
  employee: employee
  attendance: attendance
  allAttendance: attendance[]
}
