const readline = require('readline')
const { MongoClient } = require('mongodb')
require('./config')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
const admin = {
  "id": "1",
  "person_details": {
  "first_name": "admin",
  "middle_name": "a",
  "last_name": "admin",
  "birthday": "10/31/31",
  "mobile_number": "21321321",
  "email": "admin@gmail.com",
  "home_address": "adweqe"
  },
  "work_details":  {
  "designation": "Manager",
  "team": "AmihanNimble",
  "department": "TechOps",
  "supervisor": "Sherwin"
  },
  "employee_information": {
  "identification_number": "237183721",
  "position": "Probationary",
  "shift": "Flexi_10",
  "status": "Activate"
  },
  "shifts": {
  "shiftDateFrom": "Wednesday, 01-09-2019",
  "shiftDateTo": "Thursday, 01-10-2019",
  "shiftTimeStart": "10 AM",
  "shiftTimeEnd": "7 PM"
  },
  "loginCred": {
  "username": "admin",
  "password": "admin"
  }
}
rl.question('Add admin user to database. Continue? [y/n]: ', ans => {
  if(ans == 'y' || ans == 'Y') {
    MongoClient.connect(`mongodb://${process.env.DatabaseUserName}:${process.env.DatabasePass}
    	@ds${process.env.DsNumber}.mlab.com:${process.env.DatabasePort}/${process.env.DatabaseName}`,
    	{useNewUrlParser:true}, (err, dbase) => {
      console.log('  ... processing')
      let db = dbase.db(`${process.env.DatabaseName}`)
      db.collection('employees').find(admin).toArray((err, res) => {
        let collections = ['employees','employeeinformations','people','works']
        if(res[0] == undefined) {
          for(let v of collections) {
            db.createCollection(v, (err, res) => {
              console.log(`Collection ${v} created`)
            })
          }
          db.collection('employees').insertOne(admin, (err, res) => {
            console.log('added manager account')
          })
          db.collection('employeeinformations').insertOne(admin.employee_information, (err, res) => {
            console.log('employeeinformation added')
          })
          db.collection('people').insertOne(admin.person_details, (err, res) => {
            console.log('persondetails added')
          })
          db.collection('works').insertOne(admin.work_details, (err, res) => {
            console.log('work details added')
          })
        }
        else {
          console.log('done')
        }
        dbase.close()
      })
    })
    rl.close()
  }
  else {
    rl.close()
  }
})