var { GraphQLString, GraphQLEnumType, GraphQLInputObjectType, GraphQLBoolean, GraphQLObjectType, GraphQLNonNull } = require('graphql')

// INPUTS
exports.WorkDetInput = new GraphQLInputObjectType({
  name: 'InputWorkDet',
  fields: function() {
    return {
      designation: {type: exports.DesignationEnum},
      team: {type: exports.TeamEnum},
      department: {type: exports.DepartmentEnum},
      supervisor: {type: exports.SupervisorEnum}
    }
  }
})
exports.PersonDetInput = new GraphQLInputObjectType({
  name: "InputPersonDet",
  fields: function(){
    return{
      first_name: {type: GraphQLString},
      middle_name: {type: GraphQLString},
      last_name: {type: GraphQLString},
      birthday: {type: GraphQLString},
      mobile_number: {type: GraphQLString},
      email: {type: GraphQLString},
      home_address: {type: GraphQLString}
    }
  }
})
exports.LoginCredInput = new GraphQLInputObjectType({
  name: "InputLoginCred",
  fields: () => {
    return {
      username: {type: GraphQLString},
      password: {type: GraphQLString}
    }
  }
})
exports.EmployeeInformationInput = new GraphQLInputObjectType({
  name: 'InputEmployeeInformation',
  fields: function() {
    return {
      identification_number: {type: GraphQLString},
      position: {type: exports.PositionEnum},
      shift: {type: exports.ShiftEnum},
      status: {type: exports.StatusEnum}
    }
  }
})
exports.ShiftInput = new GraphQLInputObjectType({
  name: 'InputShift',
  fields: function() {
    return {
      shiftDateFrom: {type: GraphQLString},
      shiftDateTo: {type: GraphQLString},
      shiftTimeStart: {type: GraphQLString},
      shiftTimeEnd: {type: GraphQLString}
    }
  }
})
// TYPES
exports.PersonDetType = new GraphQLObjectType({
  name: "PersonDet",
  fields: function(){
    return{
      first_name: {type: GraphQLString},
      middle_name: {type: GraphQLString},
      last_name: {type: GraphQLString},
      birthday: {type: GraphQLString},
      mobile_number: {type: GraphQLString},
      email: {type: GraphQLString},
      home_address: {type: GraphQLString}
    }
  }
})
exports.WorkDetType = new GraphQLObjectType({
  name: "WorkDet",
  fields: function(){
    return{
      designation: {type: exports.DesignationEnum},
      team: {type: exports.TeamEnum},
      department: {type: exports.DepartmentEnum},
      supervisor: {type: exports.SupervisorEnum}
    }
  }
})
exports.EmployeeInformationType = new GraphQLObjectType({
  name: 'EmployeeInformationType',
  fields: function() {
    return {
      identification_number: {type: GraphQLString},
      position: {type: exports.PositionEnum},
      shift: {type: exports.ShiftEnum},
      status: {type: exports.StatusEnum}
    }
  }
})
exports.LoginCredType = new GraphQLObjectType({
  name: 'LoginCredType',
  fields: function() {
    return {
      username: {type: GraphQLString},
      password: {type: GraphQLString}
    }
  }
})
exports.EmpType = new GraphQLObjectType({
  name: 'Employee',
  fields: function() {
    return {
      id: {type: GraphQLString},
      person_details: {type: exports.PersonDetType},
      work_details: {type: exports.WorkDetType},
      employee_information: {type: exports.EmployeeInformationType},
      shifts: {type: exports.ShiftType},
      loginCred: {type: exports.LoginCredType}
    }
  }
})
exports.ShiftType = new GraphQLObjectType({
  name: 'ShiftRange',
  fields: function() {
    return {
      shiftDateFrom: {type: GraphQLString},
      shiftDateTo: {type: GraphQLString},
      shiftTimeStart: {type: GraphQLString},
      shiftTimeEnd: {type: GraphQLString}
    }
  }
})
exports.AttendanceType = new GraphQLObjectType({
  name: 'AttendanceType',
  fields: function() {
    return {
      id: {type: GraphQLString},
      attId: {type: GraphQLString},
      employee: {type: exports.PersonDetType},
      date: {type: GraphQLString},
      clockin: {type: GraphQLString},
      clockout: {type: GraphQLString},
      approved: {type: exports.ApprovedEnum},
      newClockin: {type: GraphQLString},
      newClockout: {type: GraphQLString},
      // status: {type: exports.AttendanceStatusEnum},
      role: {type: exports.DesignationEnum},
      supervisor: {type: exports.SupervisorEnum},
      late: {type: GraphQLBoolean},
      underTime: {type: GraphQLBoolean}
    }
  }
})

// ENUMS
exports.DesignationEnum = new GraphQLEnumType({
  name: "Designation",
  values: {
    Employee: {value: 'Employee'},
    Supervisor: {value: 'Supervisor'},
    HR: {value: 'HR'},
    Manager: {value: 'Manager'}
  }
})
exports.TeamEnum = new GraphQLEnumType({
  name: 'Team',
  values: {
    AmihanUnawa: {value: 'AmihanUnawa'},
    AmihanCloudBlocks: {value: 'AmihanCloudBlocks'},
    AmihanNimble: {value: 'AmihanNimble'}
  }
})
exports.DepartmentEnum = new GraphQLEnumType({
  name: 'Department',
  values: {
    ProductAndInnovation: {value: 'ProductAndInnovation'},
    ProductTeam: {value: 'ProductTeam'},
    ManagedServices: {value: 'ManagedServices'},
    TechOps: {value: 'TechOps'},
    Consulting: {value: 'Consulting'}
  }
})
const Supervisors = {}
exports.Sample = {
  Sherwin: {value: 'Sherwin'},
  Renz: {value: 'Renz'},
  Rendz: {value: 'Rendz'},
  Press: {value: 'Press'},
  Daniel: {value: 'Daniel'}
}
for(const v in exports.Sample) {
  Supervisors[v] = exports.Sample[v]
}
exports.SupervisorEnum = new GraphQLEnumType({
  name: 'Supervisor',
  values: Supervisors
})
exports.PositionEnum = new GraphQLEnumType({
  name: 'Position',
  values: {
    Regular: {value: 'Regular'},
    Probationary: {value: 'Probationary'},
    Consultant: {value: 'Consultant'}
  }
})
exports.ShiftEnum = new GraphQLEnumType({
  name: 'Shift',
  values: {
    Flexi_10: {value: 'Flexi_10'},
    Full_Flexi: {value: 'Full_Flexi'},
    Custom_Shift: {value: 'Custom_Shift'},
    Regular: {value: 'Regular'}
  }
})
exports.StatusEnum = new GraphQLEnumType({
  name: 'Status',
  values: {
    Activate: {value: 'Activate'},
    Deactivate: {value: 'Deactivate'},
  }
})
// exports.AttendanceStatusEnum = new GraphQLEnumType({
//   name: 'AttendanceStatus',
//   values: {
//     Default: {value: 'Default'},
//     Late: {value: 'Late'},
//     AWOL: {value: 'AWOL'}
//   }
// })
exports.ApprovedEnum = new GraphQLEnumType({
  name: 'Approved',
  values: {
    Approved: {value: 'Approved'},
    Not_Approved: {value: 'Not_Approved'},
    For_Approved: {value: 'For_Approved'},
    Reject: {value: 'Reject'}
  }
})

// give us feedbacks about `sili project at https://gitlab.com/amihan/blockchain/sili`

// add shifts with duration # done
// approval, modify attendance, reject # done
// add leaves with duration 
// number per leaves
