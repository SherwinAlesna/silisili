var { GraphQLString, GraphQLNonNull, GraphQLList, GraphQLObjectType } = require('graphql')
var { EmpModel, AttendanceModel, FlagAttendanceModel } = require('../models/models')
var { EmpType, AttendanceType, EmployeeInformationType, FlagAttendanceType } = require('./types')

// Query
exports.queryType = new GraphQLObjectType({
  name: 'Query',
  fields: function () {
    return {
      employees: {
        type: new GraphQLList(EmpType),
        resolve: function () {
          const emp = EmpModel.find().exec()
          if (!emp) {
            throw new Error('Error')
          }
          return emp
        }
      },
      attendance: {
        type: new GraphQLList(AttendanceType),
        args: {
          id: {type: new GraphQLNonNull(GraphQLString)}
        },
        resolve: function(root, param) {
          const att = AttendanceModel.find({id: param.id}).exec()
          if(!att) {
            throw new Error('Error')
          }
          return att
        }
      },
      allAttendance: {
        type: new GraphQLList(AttendanceType),
        resolve: function() {
          const att = AttendanceModel.find().exec()
          if(!att) {
            throw new Error('Error')
          }
          return att
        }
      },
      employee:{
        type: EmpType,
        args: {
          id: {type: new GraphQLNonNull(GraphQLString)},
        },
        resolve: function(root, param){
          const emp = EmpModel.findOne({id: param.id})
          if(!emp){
            return "Empty"
          }
          return emp
        }
      }
    }
  }
})
