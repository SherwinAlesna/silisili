var { GraphQLString, GraphQLNonNull, GraphQLBoolean } = require('graphql')
var { EmpModel, PersonModel, AttendanceModel, WorkModel, 
  EmployeeInformationModel, AttendanceModel, FlagAttendanceModel } = require('../models/models')
var { EmpType, AttendanceType, ShiftType, FlagAttendanceType, LoginCredType } = require('./types')
var { PersonDetInput, WorkDetInput, EmployeeInformationInput, ShiftInput, LoginCredInput } = require('./types')
var { ApprovedEnum, AttendanceStatusEnum, DesignationEnum, SupervisorEnum } = require('./types')

exports.addEmployee = {
  name: 'mutation',
  type: EmpType,
  args: {
    id: {type: GraphQLString},
    person_details: {type: PersonDetInput},
    work_details: {type: WorkDetInput},
    employee_information: {type: EmployeeInformationInput},
    shifts: {type: ShiftInput},
    loginCred: {type: LoginCredInput}
  },
  resolve(root, params) {
    const eModel = new EmpModel(params)
    const pModel = new PersonModel(params.person_details)
    const wModel = new WorkModel(params.work_details)
    const empInfoModel = new EmployeeInformationModel(params.employee_information)

    const newEmployee = eModel.save()
    pModel.save()
    wModel.save()
    empInfoModel.save()

    if (!newEmployee) {
      throw new Error('Error')
    }
    return newEmployee
  }
}
exports.updateEmp = {
  name: 'mutation',
  type: EmpType,
  args: {
    id: {type: GraphQLString},
    person_details: {type: PersonDetInput},
    work_details: {type: WorkDetInput},
    employee_information: {type: EmployeeInformationInput}
  },
  resolve(root, param) {
    const updateEmp = EmpModel.findOneAndUpdate(
      {id: param.id},
      {
        person_details: param.person_details,
        work_details: param.work_details,
        employee_information: param.employee_information
      },
      {new: true}
    )
    if(!updateEmp) {
      throw new Error('Error')
    }
    return updateEmp
  }
}

exports.attClockin = {
  name: 'mutation',
  type: AttendanceType,
  args: {
    id: {type: GraphQLString},
    attId: {type: GraphQLString},
    employee: {type: PersonDetInput},
    date: {type: GraphQLString},
    clockin: {type: GraphQLString},
    clockout: {type: GraphQLString},
    approved: {type: ApprovedEnum},
    newClockin: {type: GraphQLString},
    newClockout: {type: GraphQLString},
    // status: {type: AttendanceStatusEnum},
    role: {type: DesignationEnum},
    supervisor: {type: SupervisorEnum},
    late: {type: GraphQLBoolean}
  },
  resolve(root, param) {
    const aModel = new AttendanceModel(param)
    const aSave = aModel.save()

    if(!aSave) {
      throw new Error('Error')
    }
    return aSave
  }
}
exports.attClockout = {
  name: 'mutation',
  type: AttendanceType,
  args: {
    id: {type: GraphQLString},
    clockout: {type: GraphQLString},
    approved: {type: ApprovedEnum},
    underTime: {type: GraphQLBoolean}
  },
  resolve(root, params) {
    const update = AttendanceModel.findOneAndUpdate(
      {id: params.id},
      {
        clockout: params.clockout,
        underTime: params.underTime
      },
      {new: true}
    ).sort({'_id': -1})
    if(!update) {
      throw new Error('Error')
    }
    return update
  }
}
exports.newClockin = {
  name: 'mutation',
  type: AttendanceType,
  args: {
    attId: {type: GraphQLString},
    clockin: {type: GraphQLString},
    newClockin: {type: GraphQLString},
    approved: {type: ApprovedEnum}
  },
  resolve(root, param) {
    const newClockin = AttendanceModel.findOneAndUpdate(
      {attId: param.attId},
      {
        newClockin: param.newClockin,
        approved: param.approved
      },
      {new: true}
    )
    if(!newClockin) {
      throw new Error('Error')
    }
    return newClockin
  }
}
exports.newClockout = {
  name: 'mutation',
  type: AttendanceType,
  args: {
    attId: {type: GraphQLString},
    clockout: {type: GraphQLString},
    newClockout: {type: GraphQLString},
    approved: {type: ApprovedEnum},
  },
  resolve(root, param) {
    const newClockout = AttendanceModel.findOneAndUpdate(
      {attId: param.attId},
      {
        newClockout: param.newClockout,
        approved: param.approved
      },
      {new: true}
    )
    if(!newClockout) {
      throw new Error('Error')
    }
    return newClockout
  }
}

exports.updateTime = {
  name: 'mutation',
  type: AttendanceType,
  args: {
    attId: {type: GraphQLString},
    clockin: {type: GraphQLString},
    clockout: {type: GraphQLString},
    newClockin: {type: GraphQLString},
    newClockout: {type: GraphQLString},
    approved: {type: ApprovedEnum}
  },
  resolve(root, param) {
    const updateTime = AttendanceModel.findOneAndUpdate(
      {attId: param.attId},
      {
        clockin: param.newClockin,
        clockout: param.newClockout,
        newClockin: '00:00 AM',
        newClockout: '00:00 AM',
        // newClockin: param.newClockin,
        // newClockout: param.newClockout,
        approved: param.approved
      },
      {
        new: true,
        upsert: true,
        setDefaultsOnInsert: true
      }
    )
    if(!updateTime) {
      throw new Error('Error')
    }
    return updateTime
  }
}
exports.rejectAttendance = {
  name: 'mutation',
  type: AttendanceType,
  args: {
    attId: {type: GraphQLString},
    newClockin: {type: GraphQLString},
    newClockout: {type: GraphQLString},
    approved: {type: ApprovedEnum}
  },
  resolve(root, param) {
    const reject = AttendanceModel.findOneAndUpdate(
      { attId: param.attId },
      {
        newClockin: param.newClockin,
        newClockout: param.newClockout,
        approved: param.approved
      },
      { new: true }
    )
    return reject
  }
}
exports.updateAllTime = {
  name: 'mutation',
  type: AttendanceType,
  args: {
    id: {type: GraphQLString},
    newClockin: {type: GraphQLString},
    newClockout: {type: GraphQLString},
    approved: {type: ApprovedEnum}
  },
  resolve(root, param) {
    const updateAllTime = AttendanceModel.updateMany(
      {id: param.id},
      {
        newClockin: param.newClockin,
        newClockout: param.newClockout,
        approved: param.approved
      },
      {
        new: true,
        multi: true
      }
    )
    return updateAllTime
  }
}
exports.approvedAllTime = {
  name: 'mutation',
  type: AttendanceType,
  args: {
    attId: {type: GraphQLString},
    clockin: {type: GraphQLString},
    clockout: {type: GraphQLString},
    approved: {type: ApprovedEnum}
  },
  resolve(root, param) {
    const approvedTime = AttendanceModel.updateMany(
      {attId: param.attId},
      {
        clockin: param.clockin,
        clockout: param.clockout,
        newClockin: '00:00 AM',
        newClockout: '00:00 AM',
        approved: param.approved
      }, 
      {
        new: true,
        multi: true
      }
    )
    return approvedTime
  }
}

// [activate/deactivate] - [shifts]
exports.changeStatus = {
  name: 'mutation',
  type: EmpType,
  args: {
    id: {type: GraphQLString},
    employee_information: {type: EmployeeInformationInput}
  },
  resolve(root, param) {
    const status = EmpModel.findOneAndUpdate(
      {id: param.id},
      {employee_information: param.employee_information},
      {new: true}
    )
    if(!status) {
      throw new Error('Error')
    }
    return status
  }
}
exports.addShift = {
  name: 'mutation',
  type: EmpType,
  args: {
    id: {type: GraphQLString},
    shifts: {type: ShiftInput},
    employee_information: {type: EmployeeInformationInput}
  },
  resolve(root, param) {
    const addShift = EmpModel.findOneAndUpdate(
      {id: param.id},
      {
        employee_information: param.employee_information,
        shifts: param.shifts
      },
      {new: true}
    )
    if(!addShift) {
      throw new Error('error')
    }
    return addShift
  }
}
