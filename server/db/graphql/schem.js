var { GraphQLObjectType, GraphQLSchema } = require('graphql')
var queryType = require('./queries').queryType;
var mutation = require('./mutation');

exports.userSchema = new GraphQLSchema({
  query: queryType,
  mutation: new GraphQLObjectType({
    name: 'Mutation',
    fields: mutation
  })
})