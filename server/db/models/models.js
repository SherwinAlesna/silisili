var mongoose = require('mongoose')
var Schema = mongoose.Schema

var schemaPersonDet = new Schema({
  first_name: {type: String},
  middle_name: {type: String},
  last_name: {type: String},
  birthday: {type: String},
  mobile_number: {type: String},
  email: {type: String},
  home_address: {type: String}
})

var schemaWorkDet = new Schema({
  designation: {type: String},
  team: {type: String},
  department: {type: String},
  supervisor: {type: String}
})

var schemaEmployeeInformation = new Schema({
  identification_number: {type: String},
  position: {type: String},
  shift: {type: String},
  status: {type: String}
})

var schemaShift = new Schema({
  shiftDateFrom: {type: String},
  shiftDateTo: {type: String},
  shiftTimeStart: {type: String},
  shiftTimeEnd: {type: String}
})

var schemaEmployee = new Schema({
  id: {type: String},
  person_details: {type: schemaPersonDet},
  work_details: {type: schemaWorkDet},
  employee_information: {type: schemaEmployeeInformation},
  shifts: {type: schemaShift},
  loginCred: {
    username: {type: String},
    password: {type: String}
  }
})

var schemaAttendance = new Schema({
  id: {type: String},
  attId: {type: String},
  employee: {type: schemaPersonDet},
  date: {type: String},
  clockin: {type: String},
  clockout: {type: String},
  approved: {type: String},
  newClockin: {type: String},
  newClockout: {type: String},
  // status: {type: String},
  role: {type: String},
  supervisor: {type: String},
  late: {type: Boolean},
  underTime: {type: Boolean}
})

const PersonModel = mongoose.model("Person", schemaPersonDet)
const WorkModel = mongoose.model("Work", schemaWorkDet)
const EmpModel = mongoose.model('Employee', schemaEmployee)
const EmployeeInformationModel = mongoose.model('EmployeeInformation', schemaEmployeeInformation)
const AttendanceModel = mongoose.model('Attendance',schemaAttendance)
module.exports = {EmpModel,PersonModel,WorkModel, AttendanceModel, EmployeeInformationModel, AttendanceModel}
