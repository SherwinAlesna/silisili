const mongoose = require('mongoose')
require('../../config')

module.exports = mongoose.connect(`mongodb://${process.env.DatabaseUserName}:${process.env.DatabasePass}@ds${process.env.DsNumber}.mlab.com:${process.env.DatabasePort}/${process.env.DatabaseName}`,
	{useNewUrlParser:true})
