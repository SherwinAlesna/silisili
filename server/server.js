var express = require('express')
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express')
const bodyParser = require('body-parser')
var cors = require('cors')
const mongoose = require('./db/connection/mongoose') // need to fetch data
const readline = require('readline')
const { MongoClient } = require('mongodb')
const uschema = require('./db/graphql/schem').userSchema
const schema = uschema
require('./config')

var weeklyData = [
    {
        id: 1, Act: "Developing", Mon: 8, Tue: 7, Wed : 10, Thu: 0, Fri: 0, Sat: 4, Sun: 0
    },
    {
        id: 2, Act: "Project Meeting",Mon: 4, Tue: 7, Wed : 7, Thu: 0, Fri: 0, Sat: 3.2, Sun: 0
    },
    {
        id: 3, Act: "Design",Mon: 4, Tue: 2, Wed : 10, Thu: 2, Fri: 0, Sat: 0, Sun: 0
    },
    {
        id: 4, Act: "Marketing",Mon: 6, Tue: 1, Wed : 10, Thu: 0, Fri: 0, Sat: 0, Sun: 0
    }
]
// Create an express server and a GraphQL endpoint
var app = express()
app.use('*', cors())
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }))
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }))

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
const admin = {
  "id": "1",
  "person_details": {
  "first_name": "admin",
  "middle_name": "a",
  "last_name": "admin",
  "birthday": "10/31/31",
  "mobile_number": "21321321",
  "email": "admin@gmail.com",
  "home_address": "sample address"
  },
  "work_details":  {
  "designation": "Manager",
  "team": "AmihanNimble",
  "department": "TechOps",
  "supervisor": "Sherwin"
  },
  "employee_information": {
  "identification_number": "237183721",
  "position": "Probationary",
  "shift": "Flexi_10",
  "status": "Activate"
  },
  "shifts": {
  "shiftDateFrom": "Wednesday, 01-09-2019",
  "shiftDateTo": "Thursday, 01-10-2019",
  "shiftTimeStart": "10:00 AM",
  "shiftTimeEnd": "7:00 PM"
  },
  "loginCred": {
  "username": "admin",
  "password": "admin"
  }
}
rl.question('Add admin user to database. Continue? [y/n]: ', ans => {
  if(ans == 'y' || ans == 'Y') {
    MongoClient.connect(`mongodb://${process.env.DatabaseUserName}:${process.env.DatabasePass}@ds${process.env.DsNumber}.mlab.com:${process.env.DatabasePort}/${process.env.DatabaseName}`,
      {useNewUrlParser:true}, (err, dbase) => {
      console.log('  ... processing')
      let db = dbase.db(`${process.env.DatabaseName}`)
      db.collection('employees').find(admin).toArray((err, res) => {
        let collections = ['employees','employeeinformations','people','works']
        let insertInto = ['employees', 'employee_information', 'person_details', 'work_details']
        if(res[0] == undefined) {
          for(let v of collections) {
            db.createCollection(v, (err, res) => {
              console.log(`Collection ${v} created`)
            })
            // fail!.... insert todo later
            // for(let insert of insertInto) {
            //   db.collection(v).insertOne(`admin.${insert}`, (err, res) => {})
            // }
          }
          // shorten this code later
          db.collection('employees').insertOne(admin, (err, res) => {
            console.log('added manager account')
          })
          db.collection('employeeinformations').insertOne(admin.employee_information, (err, res) => {
            console.log('employeeinformation added')
          })
          db.collection('people').insertOne(admin.person_details, (err, res) => {
            console.log('persondetails added')
          })
          db.collection('works').insertOne(admin.work_details, (err, res) => {
            console.log('work details added')
            console.log('done')
          })
        }
        else {
          console.log('done')
        }
        dbase.close()
      })
    })
    rl.close()
  }
  else {
    rl.close()
  }
  app.listen(4000, () => console.log('Express GraphQL Server Now Running On localhost:4000/graphiql'))
})