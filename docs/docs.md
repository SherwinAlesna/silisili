### Manage Employee's

on this page only the HR can ``register employee``; ``change employee's shifts, activate/deactivate and modify employee``.
<p>
  <img src='./screenshots/manage.png' alt='Manage Employee'>
</p>

### Clock in/Clock out

user can clockin/clockout through ``alert`` or by clicking the ``you have not yet clocked in`` button
<p>
  <img src='./screenshots/modalClockin.png' alt='modal Clockin' width="550" height="300" />
  <img src='./screenshots/tabClockin.png' alt='drawer clockin' width="550" height="300" />
</p>

### Attendances
``Employee Attendance``

<p>
  <img src='./screenshots/employeeAttendance.png' alt='Employee Attendance'>
</p>

``Manager/Supervisor Attendance`` shown in different tab

<p>
  <img src='./screenshots/attendance.png' alt='Manager/Supervisor Attendance'>
</p>

### Edit clockin/clockout

user can edit his/her timein/timeout by clicking the time
<p>
  <img src='./screenshots/newClockinClockout.png' alt='change clockin/clokcout' />
</p>

### Approving new attendance time

supervisor/manager can approve or reject employee's new attendance
<p>
  <img src='./screenshots/approveTime.png' alt='approve time'/>
</p>
